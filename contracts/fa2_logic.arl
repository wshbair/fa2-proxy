archetype fa2_logic(fa2 : address)

import "./fa2_types.arl"

/* ERRORS ----------------------------------------------------------------- */

constant FA2_INSUFFICIENT_BALANCE : string = "FA2_INSUFFICIENT_BALANCE"
constant FA2_INVALID_AMOUNT       : string = "FA2_INVALID_AMOUNT"
constant FA2_NOT_OPERATOR         : string = "FA2_NOT_OPERATOR"
constant FA2_NOT_OWNER            : string = "FA2_NOT_OWNER"
constant FA2_TOKEN_UNDEFINED      : string = "FA2_TOKEN_UNDEFINED"
constant SIGNER_NOT_FROM          : string = "SIGNER_NOT_FROM"

/* FUNCTIONS --------------------------------------------------------------- */

function is_not_paused(s : fa2_types::Storage) : bool {
  do_fail_if(s.paused, "CONTRACT_PAUSED");
  return true
}

function check_operator(sender : address, txs : list<fa2_types::transfer_param>, s : fa2_types::Storage) : bool {
  var res = true;
  for tx in txs do
    const %from = tx.tp_from;
    const tds = tx.tp_txs;
    for td in tds do begin
      res &=
        if sender <> %from then
          (contains(s.operator, (sender, td.token_id_dest, %from)) or
           contains(s.operator_for_all, (sender, %from)))
        else
          true;
    end
    done
  done;
  return res
}

function get_from(txs : list<fa2_types::transfer_param>) : option<address> {
  match txs with
  | hd::tl -> begin
    const %from = hd.tp_from;
    for tx in tl do
      do_require(%from = tx.tp_from, FA2_NOT_OPERATOR)
    done;
    return some(%from)
  end
  | [] -> return none
  end
}

function get_balance(br : fa2_types::balance_of_request, s : fa2_types::Storage) : nat {
  do_require(contains(s.token_metadata, br.btoken_id), FA2_TOKEN_UNDEFINED);
  return (s.ledger[(br.bo_owner, br.btoken_id)] ? the : 0)
}

/* FA2 ENTRYPOINTS ----------------------------------------------------------- */

entry do_transfer(txs : list<fa2_types::transfer_param>, s : fa2_types::Storage) {
  no transfer
  called by self_address
  effect {
    for tx in txs do
      const %from = tx.tp_from;
      const tds   = tx.tp_txs;
      for td in tds do begin
        const tid = td.token_id_dest;
        do_require(contains(s.token_metadata, tid), FA2_TOKEN_UNDEFINED);
        const amount = s.ledger[(%from, tid)] ? the : 0;
        const new_amount ?=
            int_to_nat(amount - td.token_amount_dest) : FA2_INSUFFICIENT_BALANCE;
        if new_amount = 0 then
          s.ledger := remove(s.ledger, (%from, tid))
        else begin
          s.ledger := update(s.ledger, (%from, tid), some(new_amount));
        end;
        const to_amount = s.ledger[(td.to_dest, tid)] ? the : 0;
        const new_to_amount = to_amount + td.token_amount_dest;
        s.ledger := update(s.ledger, (td.to_dest, tid), some(new_to_amount));
      end done
    done;

    transfer transferred to fa2 call apply_storage<fa2_types::Storage>(s)
  }
}

entry %transfer (sender : address, p : list<fa2_types::transfer_param>, s : fa2_types::Storage) {
  no transfer
  called by self_address
  require {
    t_r1 : is_not_paused(s);
  }
  effect {
    if not check_operator(sender, p, s) then begin
      match get_from(p) with
      | some(%from) ->
        transfer 0tz to s.permits
          call consume<address * bytes * string>((%from, blake2b(pack(p)), FA2_NOT_OPERATOR))
      | none -> ()
      end
    end;

    transfer 0tz to entry self.do_transfer(p, s);
  }
}

entry update_operators (sender : address, p : list<fa2_types::update_op>, s : fa2_types::Storage) {
  no transfer
  called by self_address
  require {
    uo_r1 : is_not_paused(s);
  }
  effect {
    for up in p do
      match up with
      | add_operator(param) -> (* add *)
        do_require(param.opp_owner = sender , FA2_NOT_OWNER);
        s.operator := update(s.operator, (param.opp_operator, param.opp_token_id, param.opp_owner), some(Unit))
      | remove_operator(param) -> (* remove *)
        do_require(param.opp_owner = sender , FA2_NOT_OWNER);
        s.operator := remove(s.operator, (param.opp_operator, param.opp_token_id, param.opp_owner))
      end;
    done;

    transfer transferred to fa2 call apply_storage<fa2_types::Storage>(s)
  }
}

entry balance_of (p : fa2_types::balance_of_param, s : fa2_types::Storage) {
  const balances = map(p.requests, br ->
    fa2_types::{
      request = br;
      balance_ = get_balance(br, s)
    });

  const callback ?= address_to_contract<list<fa2_types::balance_of_response>>(p.callback) : "Invalid callback contract";
  transfer transferred to entry callback(balances)
}

/* OTHER ENTRYPOINTS --------------------------------------------------------- */

entry mint (sender : address, p : fa2_types::mint_param, s : fa2_types::Storage) {
  no transfer
  called by self_address
  require {
    m_r1 : (sender = s.owner) otherwise FA2_NOT_OWNER;
    m_r2 : is_not_paused(s);
    // m_r3 : contains(s.token_metadata, (p.mp_token_id)) otherwise FA2_TOKEN_UNDEFINED;
  }
  effect {
    const meta = ((p.mp_token_id, make_map<nat, bytes>([])));
    s.token_metadata := update(s.token_metadata, (p.mp_token_id), some(meta));
    s.ledger := update(s.ledger, (p.mp_to, p.mp_token_id), some(p.mp_token_amount));

    transfer transferred to fa2 call apply_storage<fa2_types::Storage>(s)
  }
}

/* PROXY ENTRYPOINT ---------------------------------------------------------- */

entry default (p : fa2_types::proxy_execute_param) {
  called by fa2

  effect {
    match p.pe_entrypoint with
    | Transfer -> begin
        const transfer_param ?= unpack<list<fa2_types::transfer_param>>(p.pe_params) : "UNPACK_FAILED: Invalid transfer parameters";
        transfer transferred to entry self.%transfer(p.pe_sender, transfer_param, p.pe_storage)
      end
    | Update_Operators -> begin
        const update_operators_param ?= unpack<list<fa2_types::update_op>>(p.pe_params) : "UNPACK_FAILED: Invalid update_operators parameters";
        transfer transferred to entry self.update_operators(p.pe_sender, update_operators_param, p.pe_storage)
      end
    | Balance_Of -> begin
        const balance_of_param ?= unpack<fa2_types::balance_of_param>(p.pe_params) : "UNPACK_FAILED: Invalid balance_of parameters";
        transfer transferred to entry self.balance_of(balance_of_param, p.pe_storage)
      end
    | Mint -> begin
        const mint_param ?= unpack<fa2_types::mint_param>(p.pe_params) : "UNPACK_FAILED: Invalid mint parameters";
        transfer transferred to entry self.mint(p.pe_sender, mint_param, p.pe_storage)
      end
    end
  }
}
