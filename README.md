# FA2 Proxy

Short introduction about the project. Mention the primary features and why it stands out.

## Table of Contents

1. [Overview](#overview)
2. [Architecture](#architecture)
   - [Interface Contract](#interface-contract)
   - [Logic Contract](#logic-contract)
3. [Prerequisites](#prerequisites)
4. [Installation & Deployment](#installation--deployment)
5. [Usage](#usage)
6. [Testing](#testing)

## Overview

The FA2 Proxy project is designed to separate the user-facing contract interface from the underlying logic. This separation aims to provide more flexibility in updating the contract's logic without needing to alter the interface.

## Architecture

The architecture is designed around a two-contract system: a _Proxy FA2 Contract_ and a _Logic Contract_. The _Proxy FA2 Contract_ serves as the interface for users and external contracts, compliants with the FA2 standard, while the _Logic Contract_ contains the actual business logic.

![dsds](./docs/proxy_architecture.png)

### Proxy FA2 Contract

The _Proxy FA2 Contract_ functions as an intermediary, serving as the primary point of interaction for users and external entities.

#### Storage

It adopts the standard FA2 storage model, which handles token-related data.
An extension to this standard model is the inclusion of the LOGIC_ADDRESS, which simply points to the address of the associated Logic Contract.

#### Entrypoints

The contract retains basic FA2 entrypoints like _transfer_, _balance_of_, and _update_operators_.

Additionally, it introduces specific entrypoints like _execute_ and _apply_storage_ that provide enhanced functionalities beyond the FA2 standard.

**You should not implement additional entrypoints in this contract**

### Logic Contract

The _Logic Contract_ encapsulates the primary business logic and operational rules. While it functions autonomously, it is intrinsically linked to the Proxy FA2 Contract.

#### Storage

Contains the PROXY_ADDRESS, ensuring a reference back to its associated Proxy FA2 Contract.

#### Entrypoints

The contract has just one default entrypoint which acts as a router, directing incoming requests to the appropriate function.
It hosts basic operational functions such as _transfer_, _balance_of_ and _update_operators_, in parallel with the FA2 Proxy but executing the actual logic.
It is this contract that must be modified and redeployed each time an entrypoint is added.

## Prerequisites

Before you proceed, ensure you have the following prerequisites installed:

1. **NodeJS & NPM**

   - NodeJS: >= v18.0.0
   - NPM: >= 9.0.0
   - [Download and Install with NVM](https://github.com/nvm-sh/nvm#installing-and-updating)

2. **Completium-cli**

   - Version: >= 0.4.90
   - Installation: `npm install -g completium-cli`
   - [Official Documentation](https://completium.com/docs/cli/intro)

3. **Taqueria**
   - Version: >= 0.39.0
   - [Installation Guide](https://taqueria.io/docs/getting-started/installation)

Ensure all tools are correctly installed and updated to the specified versions or later before moving forward.

## Installation & Deployment

Install dependencies:

```bash
npm i
```

Two scripts are available for deploying contracts in the scripts folder:

- [Deploy FA2](./scripts/deployFa2.ts): This script is used only once. It deploys the permits contract and FA2 proxy. You can run it with:

```bash
npm run deployFa2Proxy
```

- [Deploy FA2 Logic](./scripts/deployFa2Logic.ts): This script deploys the logic contract. It takes as arguments the address of the FA2 Proxy contract and the address of the Permits contract. You can run it with:

```bash
npm run deployFa2Logic -- <KT1_PROXY> <KT1_PERMITS>
```

## Usage

[An example script](./scripts/mintFa2Proxy.ts) shows how to interact with Taquito the proxy contract to call an entrypoint in the logic contract. You can run it with:

```bash
npm run mintFa2Proxy -- <KT1_PROXY>
```

To add an entrypoint to the Logic Contract, you need to add a new option to the default entrypoint:

```js
   | NewEntrypoint -> begin
      const new_entrypoint_param ?= unpack<fa2_types::entry_param>(p.pe_params) : "UNPACK_FAILED: Invalid entry parameters";
      transfer transferred to entry self.new_entrypoint(p.pe_sender, new_entrypoint_param, p.pe_storage)
   ends
```

Then you can create a new entrypoint with your logic. It MUST respect the conditions:

- It must only be callable by the current contract;
- Call the Proxy contract at the very end of execution;

```js
entry mint (sender : address, p : fa2_types::mint_param, s : fa2_types::Storage) {
   called by self_address

   /* Logic */

   transfer transferred to fa2 call apply_storage<fa2_types::Storage>(s)
}
```

You'll then need to regenerate the types.

For Archetype: `npm run gen-binding`

For Taqueria: `taq generate types`

## Testing

Before running the tests, if any changes have been made to the contracts, execute the following command to generate new bindings:

```bash
npm run gen-binding
```

After generating the bindings, you can proceed to run all tests using the following command:

```bash
npm run test
```

## License

This project is licensed under the MIT License. For detailed information, please refer to the [LICENSE](./LICENSE) file in the repository.
