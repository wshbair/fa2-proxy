import { get_account, set_mockup, set_quiet } from "@completium/experiment-ts";
import assert from "assert";

import { fa2_proxy } from "./binding/fa2_proxy";
import { fa2_logic } from "./binding/fa2_logic";
import { permits } from "./binding/permits";

const alice = get_account("alice"); /* Account */
set_mockup(); /* Endpoint */
set_quiet(true); /* Verbose mode */

/* Scenarios --------------------------------------------------------------- */

describe("[FA2 Proxy - Multi-Asset] Contracts deployment", async () => {
  it("Permits contract deployment should succeed", async () => {
    await permits.deploy(alice.get_address(), { as: alice });
    assert(permits.get_address() !== undefined);
  });

  it("FA2 Proxy Contract multi-asset contract deployment should succeed", async () => {
    await fa2_proxy.deploy(alice.get_address(), permits.get_address(), { as: alice });
    assert(fa2_proxy.get_address() !== undefined);
  });

  it("FA2 Logic Contract deployment should succeed", async () => {
    await fa2_logic.deploy(fa2_proxy.get_address(), { as: alice });
    assert(fa2_logic.get_address() !== undefined);
  });

  it("Add Logic Contract address to FA2 Proxy multi-asset contract should succeed", async () => {
    await fa2_proxy.set_proxy_address(fa2_logic.get_address(), { as: alice });
    // assert(fa2_logic.get_address() !== undefined);
  });
});
