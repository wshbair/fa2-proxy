import { Address, Nat } from "@completium/archetype-ts-types";
import {
  expect_to_fail,
  get_account,
  pack,
  set_mockup,
  set_quiet,
} from "@completium/experiment-ts";
import assert from "assert";

import {
  fa2_proxy,
  Mint,
  add_operator,
  exec_param,
  fa2_types__operator_param,
  remove_operator,
} from "./binding/fa2_proxy";
import { fa2_logic } from "./binding/fa2_logic";
import { add, permits } from "./binding/permits";
import { mint_param, mint_param_mich_type, operator_param } from "./binding/fa2_types";

const alice = get_account("alice"); /* Account */
const bob = get_account("bob"); /* Account */
set_mockup(); /* Endpoint */
set_quiet(true); /* Verbose mode */
const amount = new Nat(1000); /* Amount */
const token_id = new Nat(0); /* Token ID */

before(async () => {
  await permits.deploy(alice.get_address(), { as: alice });
  await fa2_proxy.deploy(alice.get_address(), permits.get_address(), {
    as: alice,
  });
  await fa2_logic.deploy(fa2_proxy.get_address(), { as: alice });

  await permits.manage_consumer(new add(fa2_logic.get_address()), { as: alice });
  await fa2_proxy.set_proxy_address(fa2_logic.get_address(), { as: alice });
  await fa2_proxy.exec(
    new exec_param(
      new Mint(),
      pack(new mint_param(alice.get_address(), token_id, amount).to_mich(), mint_param_mich_type),
    ),
    { as: alice },
  );
});

describe("[FA2 Proxy - Multi-Asset] Update Operators", async () => {
  it("Add an operator for ourself should succeed", async () => {
    const op_key: [Address, Nat, Address] = [bob.get_address(), token_id, alice.get_address()];

    const has_operator_before = await fa2_proxy.has_operator_value(op_key);
    assert(has_operator_before == false);

    await fa2_proxy.update_operators(
      [
        new add_operator(
          new fa2_types__operator_param(alice.get_address(), bob.get_address(), token_id),
        ),
      ],
      { as: alice },
    );

    const has_operator_after = await fa2_proxy.has_operator_value(op_key);
    assert(has_operator_after == true);
  });

  it("Remove a non existing operator should succeed", async () => {
    await fa2_proxy.update_operators(
      [
        new add_operator(
          new fa2_types__operator_param(alice.get_address(), bob.get_address(), token_id),
        ),
      ],
      { as: alice },
    );
  });

  it("Remove an existing operator for another user should fail", async () => {
    await expect_to_fail(async () => {
      await fa2_proxy.update_operators(
        [
          new remove_operator(
            new operator_param(alice.get_address(), fa2_proxy.get_address(), token_id),
          ),
        ],
        { as: bob },
      );
    }, fa2_logic.errors.FA2_NOT_OWNER);
  });

  it("Add operator for another user should fail", async () => {
    await expect_to_fail(async () => {
      await fa2_proxy.update_operators(
        [
          new add_operator(
            new operator_param(bob.get_address(), fa2_proxy.get_address(), token_id),
          ),
        ],
        { as: alice },
      );
    }, fa2_logic.errors.FA2_NOT_OWNER);
  });

  it("Remove an existing operator should succeed", async () => {
    const uo_key: [Address, Nat, Address] = [
      fa2_proxy.get_address(),
      token_id,
      alice.get_address(),
    ];

    const has_operator_before = await fa2_proxy.has_operator_value(uo_key);
    assert(has_operator_before == true);
    await fa2_proxy.update_operators(
      [
        new remove_operator(
          new operator_param(alice.get_address(), fa2_proxy.get_address(), token_id),
        ),
      ],
      { as: alice },
    );
    const has_operator_after = await fa2_proxy.has_operator_value(uo_key);
    assert(has_operator_after == false);
  });
});
