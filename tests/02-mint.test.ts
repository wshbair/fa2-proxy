import { Nat } from "@completium/archetype-ts-types";
import {
  expect_to_fail,
  get_account,
  pack,
  set_mockup,
  set_quiet,
} from "@completium/experiment-ts";
import assert from "assert";

import { fa2_proxy, Mint, exec_param } from "./binding/fa2_proxy";
import { fa2_logic } from "./binding/fa2_logic";
import { permits } from "./binding/permits";
import { mint_param, mint_param_mich_type } from "./binding/fa2_types";

const alice = get_account("alice"); /* Account */
const bob = get_account("bob"); /* Account */
set_mockup(); /* Endpoint */
set_quiet(true); /* Verbose mode */

before(async () => {
  await permits.deploy(alice.get_address(), { as: alice });
  await fa2_proxy.deploy(alice.get_address(), permits.get_address(), {
    as: alice,
  });
  await fa2_logic.deploy(fa2_proxy.get_address(), { as: alice });
  await fa2_proxy.set_proxy_address(fa2_logic.get_address(), { as: alice });
});

describe("[FA2 Proxy - Multi-Asset] Minting", async () => {
  it("Mint tokens as owner for ourself should succeed", async () => {
    const amount = new Nat(1000);
    const token_id = new Nat(0);
    const args = pack(
      new mint_param(
        alice.get_address(), // owner
        token_id, // token id
        amount, // amount
      ).to_mich(),
      mint_param_mich_type,
    );

    await fa2_proxy.exec(new exec_param(new Mint(), args), { as: alice });
  });

  it("Mint tokens as non owner for ourself should fail", async () => {
    const amount = new Nat(1000);
    const token_id = new Nat(0);
    const args = pack(
      new mint_param(
        alice.get_address(), // owner
        token_id, // token id
        amount, // amount
      ).to_mich(),
      mint_param_mich_type,
    );

    await expect_to_fail(async () => {
      await fa2_proxy.exec(new exec_param(new Mint(), args), { as: bob });
    }, fa2_logic.errors.FA2_NOT_OWNER);
  });

  it("Mint tokens as non owner for someone else should fail", async () => {
    const amount = new Nat(1000);
    const token_id = new Nat(0);
    const args = pack(
      new mint_param(
        alice.get_address(), // owner
        token_id, // token id
        amount, // amount
      ).to_mich(),
      mint_param_mich_type,
    );

    await expect_to_fail(async () => {
      await fa2_proxy.exec(new exec_param(new Mint(), args), { as: bob });
    }, fa2_logic.errors.FA2_NOT_OWNER);
  });

  it("Mint tokens as owner for someone else should succeed", async () => {
    const amount = new Nat(1000);
    const token_id = new Nat(1);

    const balance_before = await fa2_proxy.get_ledger_value([bob.get_address(), token_id]);
    assert(balance_before == undefined);

    await fa2_proxy.exec(
      new exec_param(
        new Mint(),
        pack(
          new mint_param(
            bob.get_address(), // owner
            token_id, // token id
            amount, // amount
          ).to_mich(),
          mint_param_mich_type,
        ),
      ),
      { as: alice },
    );

    const balance_after = await fa2_proxy.get_ledger_value([bob.get_address(), token_id]);
    assert(balance_after?.equals(amount));
  });
});
