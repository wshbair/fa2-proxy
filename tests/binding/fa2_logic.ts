import * as ex from "@completium/experiment-ts";
import * as att from "@completium/archetype-ts-types";
export enum fa2_types__e_entrypoints_types {
  Transfer = "Transfer",
  Update_Operators = "Update_Operators",
  Balance_Of = "Balance_Of",
  Mint = "Mint",
}
export abstract class fa2_types__e_entrypoints extends att.Enum<fa2_types__e_entrypoints_types> {
  abstract to_mich(): att.Micheline;
  equals(v: fa2_types__e_entrypoints): boolean {
    return att.micheline_equals(this.to_mich(), v.to_mich());
  }
}
export class Transfer extends fa2_types__e_entrypoints {
  constructor() {
    super(fa2_types__e_entrypoints_types.Transfer);
  }
  to_mich() {
    return new att.Int(0).to_mich();
  }
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
}
export class Update_Operators extends fa2_types__e_entrypoints {
  constructor() {
    super(fa2_types__e_entrypoints_types.Update_Operators);
  }
  to_mich() {
    return new att.Int(1).to_mich();
  }
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
}
export class Balance_Of extends fa2_types__e_entrypoints {
  constructor() {
    super(fa2_types__e_entrypoints_types.Balance_Of);
  }
  to_mich() {
    return new att.Int(2).to_mich();
  }
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
}
export class Mint extends fa2_types__e_entrypoints {
  constructor() {
    super(fa2_types__e_entrypoints_types.Mint);
  }
  to_mich() {
    return new att.Int(3).to_mich();
  }
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
}
export enum fa2_types__update_op_types {
  add_operator = "add_operator",
  remove_operator = "remove_operator",
}
export abstract class fa2_types__update_op extends att.Enum<fa2_types__update_op_types> {
  abstract to_mich(): att.Micheline;
  equals(v: fa2_types__update_op): boolean {
    return att.micheline_equals(this.to_mich(), v.to_mich());
  }
}
export class add_operator extends fa2_types__update_op {
  constructor(private content: fa2_types__operator_param) {
    super(fa2_types__update_op_types.add_operator);
  }
  to_mich() {
    return att.left_to_mich(this.content.to_mich());
  }
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
  get() {
    return this.content;
  }
}
export class remove_operator extends fa2_types__update_op {
  constructor(private content: fa2_types__operator_param) {
    super(fa2_types__update_op_types.remove_operator);
  }
  to_mich() {
    return att.right_to_mich(this.content.to_mich());
  }
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
  get() {
    return this.content;
  }
}
export const mich_to_fa2_types__e_entrypoints = (m: any): fa2_types__e_entrypoints => {
  const v = new att.Nat((m as att.Mint).int).to_big_number().toNumber();
  switch (v) {
    case 0:
      return new Transfer();
    case 1:
      return new Update_Operators();
    case 2:
      return new Balance_Of();
    case 3:
      return new Mint();
    default:
      throw new Error("mich_to_asset_type : invalid value " + v);
  }
};
export const mich_to_fa2_types__update_op = (m: att.Micheline): fa2_types__update_op => {
  if ((m as att.Msingle).prim == "Left") {
    return new add_operator(fa2_types__operator_param.from_mich((m as att.Msingle).args[0]));
  }
  if ((m as att.Msingle).prim == "Right") {
    return new remove_operator(fa2_types__operator_param.from_mich((m as att.Msingle).args[0]));
  }
  throw new Error("mich_to_fa2_types__update_op : invalid micheline");
};
export class fa2_types__Storage implements att.ArchetypeType {
  constructor(
    public owner: att.Address,
    public permits: att.Address,
    public token_metadata: Array<[att.Nat, [att.Nat, Array<[att.Nat, att.Bytes]>]]>,
    public ledger: Array<[[att.Address, att.Nat], att.Nat]>,
    public operator: Array<[[att.Address, att.Nat, att.Address], att.Unit]>,
    public operator_for_all: Array<[[att.Address, att.Address], att.Unit]>,
    public paused: boolean,
  ) {}
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
  to_mich(): att.Micheline {
    return att.pair_to_mich([
      this.owner.to_mich(),
      this.permits.to_mich(),
      att.list_to_mich(this.token_metadata, x => {
        const x_key = x[0];
        const x_value = x[1];
        return att.elt_to_mich(
          x_key.to_mich(),
          att.pair_to_mich([
            x_value[0].to_mich(),
            att.list_to_mich(x_value[1], x => {
              const x_key = x[0];
              const x_value = x[1];
              return att.elt_to_mich(x_key.to_mich(), x_value.to_mich());
            }),
          ]),
        );
      }),
      att.list_to_mich(this.ledger, x => {
        const x_key = x[0];
        const x_value = x[1];
        return att.elt_to_mich(
          att.pair_to_mich([x_key[0].to_mich(), x_key[1].to_mich()]),
          x_value.to_mich(),
        );
      }),
      att.list_to_mich(this.operator, x => {
        const x_key = x[0];
        const x_value = x[1];
        return att.elt_to_mich(
          att.pair_to_mich([x_key[0].to_mich(), x_key[1].to_mich(), x_key[2].to_mich()]),
          att.unit_to_mich(),
        );
      }),
      att.list_to_mich(this.operator_for_all, x => {
        const x_key = x[0];
        const x_value = x[1];
        return att.elt_to_mich(
          att.pair_to_mich([x_key[0].to_mich(), x_key[1].to_mich()]),
          att.unit_to_mich(),
        );
      }),
      att.bool_to_mich(this.paused),
    ]);
  }
  equals(v: fa2_types__Storage): boolean {
    return att.micheline_equals(this.to_mich(), v.to_mich());
  }
  static from_mich(input: att.Micheline): fa2_types__Storage {
    return new fa2_types__Storage(
      att.Address.from_mich((input as att.Mpair).args[0]),
      att.Address.from_mich((input as att.Mpair).args[1]),
      att.Int.from_mich((input as att.Mpair).args[2]) as any,
      att.Int.from_mich((input as att.Mpair).args[3]) as any,
      att.Int.from_mich((input as att.Mpair).args[4]) as any,
      att.Int.from_mich((input as att.Mpair).args[5]) as any,
      att.mich_to_bool((input as att.Mpair).args[6]),
    );
  }
}
export class fa2_types__proxy_execute_param implements att.ArchetypeType {
  constructor(
    public pe_entrypoint: fa2_types__e_entrypoints,
    public pe_sender: att.Address,
    public pe_params: att.Bytes,
    public pe_storage: fa2_types__Storage,
  ) {}
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
  to_mich(): att.Micheline {
    return att.pair_to_mich([
      this.pe_entrypoint.to_mich(),
      this.pe_sender.to_mich(),
      this.pe_params.to_mich(),
      this.pe_storage.to_mich(),
    ]);
  }
  equals(v: fa2_types__proxy_execute_param): boolean {
    return att.micheline_equals(this.to_mich(), v.to_mich());
  }
  static from_mich(input: att.Micheline): fa2_types__proxy_execute_param {
    return new fa2_types__proxy_execute_param(
      mich_to_fa2_types__e_entrypoints((input as att.Mpair).args[0]),
      att.Address.from_mich((input as att.Mpair).args[1]),
      att.Bytes.from_mich((input as att.Mpair).args[2]),
      fa2_types__Storage.from_mich(
        att.pair_to_mich((input as att.Mpair as att.Mpair).args.slice(3, 10)),
      ),
    );
  }
}
export class fa2_types__transfer_destination implements att.ArchetypeType {
  constructor(
    public to_dest: att.Address,
    public token_id_dest: att.Nat,
    public token_amount_dest: att.Nat,
  ) {}
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
  to_mich(): att.Micheline {
    return att.pair_to_mich([
      this.to_dest.to_mich(),
      att.pair_to_mich([this.token_id_dest.to_mich(), this.token_amount_dest.to_mich()]),
    ]);
  }
  equals(v: fa2_types__transfer_destination): boolean {
    return att.micheline_equals(this.to_mich(), v.to_mich());
  }
  static from_mich(input: att.Micheline): fa2_types__transfer_destination {
    return new fa2_types__transfer_destination(
      att.Address.from_mich((input as att.Mpair).args[0]),
      att.Nat.from_mich(
        (att.pair_to_mich((input as att.Mpair as att.Mpair).args.slice(1, 3)) as att.Mpair).args[0],
      ),
      att.Nat.from_mich(
        (att.pair_to_mich((input as att.Mpair as att.Mpair).args.slice(1, 3)) as att.Mpair).args[1],
      ),
    );
  }
}
export class fa2_types__transfer_param implements att.ArchetypeType {
  constructor(
    public tp_from: att.Address,
    public tp_txs: Array<fa2_types__transfer_destination>,
  ) {}
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
  to_mich(): att.Micheline {
    return att.pair_to_mich([
      this.tp_from.to_mich(),
      att.list_to_mich(this.tp_txs, x => {
        return x.to_mich();
      }),
    ]);
  }
  equals(v: fa2_types__transfer_param): boolean {
    return att.micheline_equals(this.to_mich(), v.to_mich());
  }
  static from_mich(input: att.Micheline): fa2_types__transfer_param {
    return new fa2_types__transfer_param(
      att.Address.from_mich((input as att.Mpair).args[0]),
      att.mich_to_list((input as att.Mpair).args[1], x => {
        return fa2_types__transfer_destination.from_mich(x);
      }),
    );
  }
}
export class fa2_types__balance_of_request implements att.ArchetypeType {
  constructor(
    public bo_owner: att.Address,
    public btoken_id: att.Nat,
  ) {}
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
  to_mich(): att.Micheline {
    return att.pair_to_mich([this.bo_owner.to_mich(), this.btoken_id.to_mich()]);
  }
  equals(v: fa2_types__balance_of_request): boolean {
    return att.micheline_equals(this.to_mich(), v.to_mich());
  }
  static from_mich(input: att.Micheline): fa2_types__balance_of_request {
    return new fa2_types__balance_of_request(
      att.Address.from_mich((input as att.Mpair).args[0]),
      att.Nat.from_mich((input as att.Mpair).args[1]),
    );
  }
}
export class fa2_types__balance_of_response implements att.ArchetypeType {
  constructor(
    public request: fa2_types__balance_of_request,
    public balance_: att.Nat,
  ) {}
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
  to_mich(): att.Micheline {
    return att.pair_to_mich([this.request.to_mich(), this.balance_.to_mich()]);
  }
  equals(v: fa2_types__balance_of_response): boolean {
    return att.micheline_equals(this.to_mich(), v.to_mich());
  }
  static from_mich(input: att.Micheline): fa2_types__balance_of_response {
    return new fa2_types__balance_of_response(
      fa2_types__balance_of_request.from_mich((input as att.Mpair).args[0]),
      att.Nat.from_mich((input as att.Mpair).args[1]),
    );
  }
}
export class fa2_types__balance_of_param implements att.ArchetypeType {
  constructor(
    public requests: Array<fa2_types__balance_of_request>,
    public callback: att.Address,
  ) {}
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
  to_mich(): att.Micheline {
    return att.pair_to_mich([
      att.list_to_mich(this.requests, x => {
        return x.to_mich();
      }),
      this.callback.to_mich(),
    ]);
  }
  equals(v: fa2_types__balance_of_param): boolean {
    return att.micheline_equals(this.to_mich(), v.to_mich());
  }
  static from_mich(input: att.Micheline): fa2_types__balance_of_param {
    return new fa2_types__balance_of_param(
      att.mich_to_list((input as att.Mpair).args[0], x => {
        return fa2_types__balance_of_request.from_mich(x);
      }),
      att.Address.from_mich((input as att.Mpair).args[1]),
    );
  }
}
export class fa2_types__operator_param implements att.ArchetypeType {
  constructor(
    public opp_owner: att.Address,
    public opp_operator: att.Address,
    public opp_token_id: att.Nat,
  ) {}
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
  to_mich(): att.Micheline {
    return att.pair_to_mich([
      this.opp_owner.to_mich(),
      att.pair_to_mich([this.opp_operator.to_mich(), this.opp_token_id.to_mich()]),
    ]);
  }
  equals(v: fa2_types__operator_param): boolean {
    return att.micheline_equals(this.to_mich(), v.to_mich());
  }
  static from_mich(input: att.Micheline): fa2_types__operator_param {
    return new fa2_types__operator_param(
      att.Address.from_mich((input as att.Mpair).args[0]),
      att.Address.from_mich(
        (att.pair_to_mich((input as att.Mpair as att.Mpair).args.slice(1, 3)) as att.Mpair).args[0],
      ),
      att.Nat.from_mich(
        (att.pair_to_mich((input as att.Mpair as att.Mpair).args.slice(1, 3)) as att.Mpair).args[1],
      ),
    );
  }
}
export class fa2_types__mint_param implements att.ArchetypeType {
  constructor(
    public mp_to: att.Address,
    public mp_token_id: att.Nat,
    public mp_token_amount: att.Nat,
  ) {}
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
  to_mich(): att.Micheline {
    return att.pair_to_mich([
      this.mp_to.to_mich(),
      this.mp_token_id.to_mich(),
      this.mp_token_amount.to_mich(),
    ]);
  }
  equals(v: fa2_types__mint_param): boolean {
    return att.micheline_equals(this.to_mich(), v.to_mich());
  }
  static from_mich(input: att.Micheline): fa2_types__mint_param {
    return new fa2_types__mint_param(
      att.Address.from_mich((input as att.Mpair).args[0]),
      att.Nat.from_mich((input as att.Mpair).args[1]),
      att.Nat.from_mich((input as att.Mpair).args[2]),
    );
  }
}
export const fa2_types__Storage_mich_type: att.MichelineType = att.pair_array_to_mich_type(
  [
    att.prim_annot_to_mich_type("address", ["%owner"]),
    att.prim_annot_to_mich_type("address", ["%permits"]),
    att.pair_annot_to_mich_type(
      "big_map",
      att.prim_annot_to_mich_type("nat", []),
      att.pair_array_to_mich_type(
        [
          att.prim_annot_to_mich_type("nat", []),
          att.pair_annot_to_mich_type(
            "map",
            att.prim_annot_to_mich_type("nat", []),
            att.prim_annot_to_mich_type("bytes", []),
            [],
          ),
        ],
        [],
      ),
      ["%token_metadata"],
    ),
    att.pair_annot_to_mich_type(
      "big_map",
      att.pair_array_to_mich_type(
        [att.prim_annot_to_mich_type("address", []), att.prim_annot_to_mich_type("nat", [])],
        [],
      ),
      att.prim_annot_to_mich_type("nat", []),
      ["%ledger"],
    ),
    att.pair_annot_to_mich_type(
      "big_map",
      att.pair_array_to_mich_type(
        [
          att.prim_annot_to_mich_type("address", []),
          att.prim_annot_to_mich_type("nat", []),
          att.prim_annot_to_mich_type("address", []),
        ],
        [],
      ),
      att.prim_annot_to_mich_type("unit", []),
      ["%operator"],
    ),
    att.pair_annot_to_mich_type(
      "big_map",
      att.pair_array_to_mich_type(
        [att.prim_annot_to_mich_type("address", []), att.prim_annot_to_mich_type("address", [])],
        [],
      ),
      att.prim_annot_to_mich_type("unit", []),
      ["%operator_for_all"],
    ),
    att.prim_annot_to_mich_type("bool", ["%paused"]),
  ],
  [],
);
export const fa2_types__proxy_execute_param_mich_type: att.MichelineType =
  att.pair_array_to_mich_type(
    [
      att.prim_annot_to_mich_type("int", ["%pe_entrypoint"]),
      att.prim_annot_to_mich_type("address", ["%pe_sender"]),
      att.prim_annot_to_mich_type("bytes", ["%pe_params"]),
      att.pair_array_to_mich_type(
        [
          att.prim_annot_to_mich_type("address", ["%owner"]),
          att.prim_annot_to_mich_type("address", ["%permits"]),
          att.pair_annot_to_mich_type(
            "big_map",
            att.prim_annot_to_mich_type("nat", []),
            att.pair_array_to_mich_type(
              [
                att.prim_annot_to_mich_type("nat", []),
                att.pair_annot_to_mich_type(
                  "map",
                  att.prim_annot_to_mich_type("nat", []),
                  att.prim_annot_to_mich_type("bytes", []),
                  [],
                ),
              ],
              [],
            ),
            ["%token_metadata"],
          ),
          att.pair_annot_to_mich_type(
            "big_map",
            att.pair_array_to_mich_type(
              [att.prim_annot_to_mich_type("address", []), att.prim_annot_to_mich_type("nat", [])],
              [],
            ),
            att.prim_annot_to_mich_type("nat", []),
            ["%ledger"],
          ),
          att.pair_annot_to_mich_type(
            "big_map",
            att.pair_array_to_mich_type(
              [
                att.prim_annot_to_mich_type("address", []),
                att.prim_annot_to_mich_type("nat", []),
                att.prim_annot_to_mich_type("address", []),
              ],
              [],
            ),
            att.prim_annot_to_mich_type("unit", []),
            ["%operator"],
          ),
          att.pair_annot_to_mich_type(
            "big_map",
            att.pair_array_to_mich_type(
              [
                att.prim_annot_to_mich_type("address", []),
                att.prim_annot_to_mich_type("address", []),
              ],
              [],
            ),
            att.prim_annot_to_mich_type("unit", []),
            ["%operator_for_all"],
          ),
          att.prim_annot_to_mich_type("bool", ["%paused"]),
        ],
        ["%pe_storage"],
      ),
    ],
    [],
  );
export const fa2_types__transfer_destination_mich_type: att.MichelineType =
  att.pair_array_to_mich_type(
    [
      att.prim_annot_to_mich_type("address", ["%to_"]),
      att.pair_array_to_mich_type(
        [
          att.prim_annot_to_mich_type("nat", ["%token_id"]),
          att.prim_annot_to_mich_type("nat", ["%amount"]),
        ],
        [],
      ),
    ],
    [],
  );
export const fa2_types__transfer_param_mich_type: att.MichelineType = att.pair_array_to_mich_type(
  [
    att.prim_annot_to_mich_type("address", ["%from_"]),
    att.list_annot_to_mich_type(
      att.pair_array_to_mich_type(
        [
          att.prim_annot_to_mich_type("address", ["%to_"]),
          att.pair_array_to_mich_type(
            [
              att.prim_annot_to_mich_type("nat", ["%token_id"]),
              att.prim_annot_to_mich_type("nat", ["%amount"]),
            ],
            [],
          ),
        ],
        [],
      ),
      ["%txs"],
    ),
  ],
  [],
);
export const fa2_types__balance_of_request_mich_type: att.MichelineType =
  att.pair_array_to_mich_type(
    [
      att.prim_annot_to_mich_type("address", ["%owner"]),
      att.prim_annot_to_mich_type("nat", ["%token_id"]),
    ],
    [],
  );
export const fa2_types__balance_of_response_mich_type: att.MichelineType =
  att.pair_array_to_mich_type(
    [
      att.pair_array_to_mich_type(
        [
          att.prim_annot_to_mich_type("address", ["%owner"]),
          att.prim_annot_to_mich_type("nat", ["%token_id"]),
        ],
        ["%request"],
      ),
      att.prim_annot_to_mich_type("nat", ["%balance"]),
    ],
    [],
  );
export const fa2_types__balance_of_param_mich_type: att.MichelineType = att.pair_array_to_mich_type(
  [
    att.list_annot_to_mich_type(
      att.pair_array_to_mich_type(
        [
          att.prim_annot_to_mich_type("address", ["%owner"]),
          att.prim_annot_to_mich_type("nat", ["%token_id"]),
        ],
        [],
      ),
      ["%request"],
    ),
    att.prim_annot_to_mich_type("address", ["%callback"]),
  ],
  [],
);
export const fa2_types__operator_param_mich_type: att.MichelineType = att.pair_array_to_mich_type(
  [
    att.prim_annot_to_mich_type("address", ["%owner"]),
    att.pair_array_to_mich_type(
      [
        att.prim_annot_to_mich_type("address", ["%operator"]),
        att.prim_annot_to_mich_type("nat", ["%token_id"]),
      ],
      [],
    ),
  ],
  [],
);
export const fa2_types__mint_param_mich_type: att.MichelineType = att.pair_array_to_mich_type(
  [
    att.prim_annot_to_mich_type("address", ["%mp_to"]),
    att.prim_annot_to_mich_type("nat", ["%mp_token_id"]),
    att.prim_annot_to_mich_type("nat", ["%mp_token_amount"]),
  ],
  [],
);
const do_transfer_arg_to_mich = (
  txs: Array<fa2_types__transfer_param>,
  s: fa2_types__Storage,
): att.Micheline => {
  return att.pair_to_mich([
    att.list_to_mich(txs, x => {
      return x.to_mich();
    }),
    s.to_mich(),
  ]);
};
const transfer_arg_to_mich = (
  sender: att.Address,
  p: Array<fa2_types__transfer_param>,
  s: fa2_types__Storage,
): att.Micheline => {
  return att.pair_to_mich([
    sender.to_mich(),
    att.list_to_mich(p, x => {
      return x.to_mich();
    }),
    s.to_mich(),
  ]);
};
const update_operators_arg_to_mich = (
  sender: att.Address,
  p: Array<fa2_types__update_op>,
  s: fa2_types__Storage,
): att.Micheline => {
  return att.pair_to_mich([
    sender.to_mich(),
    att.list_to_mich(p, x => {
      return x.to_mich();
    }),
    s.to_mich(),
  ]);
};
const balance_of_arg_to_mich = (
  p: fa2_types__balance_of_param,
  s: fa2_types__Storage,
): att.Micheline => {
  return att.pair_to_mich([p.to_mich(), s.to_mich()]);
};
const mint_arg_to_mich = (
  sender: att.Address,
  p: fa2_types__mint_param,
  s: fa2_types__Storage,
): att.Micheline => {
  return att.pair_to_mich([sender.to_mich(), p.to_mich(), s.to_mich()]);
};
const default_arg_to_mich = (p: fa2_types__proxy_execute_param): att.Micheline => {
  return p.to_mich();
};
export class Fa2_logic {
  address: string | undefined;
  constructor(address: string | undefined = undefined) {
    this.address = address;
  }
  get_address(): att.Address {
    if (undefined != this.address) {
      return new att.Address(this.address);
    }
    throw new Error("Contract not initialised");
  }
  async get_balance(): Promise<att.Tez> {
    if (null != this.address) {
      return await ex.get_balance(new att.Address(this.address));
    }
    throw new Error("Contract not initialised");
  }
  async deploy(fa2: att.Address, params: Partial<ex.Parameters>) {
    const address = (
      await ex.deploy(
        "./contracts/fa2_logic.arl",
        {
          fa2: fa2.to_mich(),
        },
        params,
      )
    ).address;
    this.address = address;
  }
  async do_transfer(
    txs: Array<fa2_types__transfer_param>,
    s: fa2_types__Storage,
    params: Partial<ex.Parameters>,
  ): Promise<att.CallResult> {
    if (this.address != undefined) {
      return await ex.call(this.address, "do_transfer", do_transfer_arg_to_mich(txs, s), params);
    }
    throw new Error("Contract not initialised");
  }
  async transfer(
    sender: att.Address,
    p: Array<fa2_types__transfer_param>,
    s: fa2_types__Storage,
    params: Partial<ex.Parameters>,
  ): Promise<att.CallResult> {
    if (this.address != undefined) {
      return await ex.call(this.address, "transfer", transfer_arg_to_mich(sender, p, s), params);
    }
    throw new Error("Contract not initialised");
  }
  async update_operators(
    sender: att.Address,
    p: Array<fa2_types__update_op>,
    s: fa2_types__Storage,
    params: Partial<ex.Parameters>,
  ): Promise<att.CallResult> {
    if (this.address != undefined) {
      return await ex.call(
        this.address,
        "update_operators",
        update_operators_arg_to_mich(sender, p, s),
        params,
      );
    }
    throw new Error("Contract not initialised");
  }
  async balance_of(
    p: fa2_types__balance_of_param,
    s: fa2_types__Storage,
    params: Partial<ex.Parameters>,
  ): Promise<att.CallResult> {
    if (this.address != undefined) {
      return await ex.call(this.address, "balance_of", balance_of_arg_to_mich(p, s), params);
    }
    throw new Error("Contract not initialised");
  }
  async mint(
    sender: att.Address,
    p: fa2_types__mint_param,
    s: fa2_types__Storage,
    params: Partial<ex.Parameters>,
  ): Promise<att.CallResult> {
    if (this.address != undefined) {
      return await ex.call(this.address, "mint", mint_arg_to_mich(sender, p, s), params);
    }
    throw new Error("Contract not initialised");
  }
  async default(
    p: fa2_types__proxy_execute_param,
    params: Partial<ex.Parameters>,
  ): Promise<att.CallResult> {
    if (this.address != undefined) {
      return await ex.call(this.address, "default", default_arg_to_mich(p), params);
    }
    throw new Error("Contract not initialised");
  }
  async get_do_transfer_param(
    txs: Array<fa2_types__transfer_param>,
    s: fa2_types__Storage,
    params: Partial<ex.Parameters>,
  ): Promise<att.CallParameter> {
    if (this.address != undefined) {
      return await ex.get_call_param(
        this.address,
        "do_transfer",
        do_transfer_arg_to_mich(txs, s),
        params,
      );
    }
    throw new Error("Contract not initialised");
  }
  async get_transfer_param(
    sender: att.Address,
    p: Array<fa2_types__transfer_param>,
    s: fa2_types__Storage,
    params: Partial<ex.Parameters>,
  ): Promise<att.CallParameter> {
    if (this.address != undefined) {
      return await ex.get_call_param(
        this.address,
        "transfer",
        transfer_arg_to_mich(sender, p, s),
        params,
      );
    }
    throw new Error("Contract not initialised");
  }
  async get_update_operators_param(
    sender: att.Address,
    p: Array<fa2_types__update_op>,
    s: fa2_types__Storage,
    params: Partial<ex.Parameters>,
  ): Promise<att.CallParameter> {
    if (this.address != undefined) {
      return await ex.get_call_param(
        this.address,
        "update_operators",
        update_operators_arg_to_mich(sender, p, s),
        params,
      );
    }
    throw new Error("Contract not initialised");
  }
  async get_balance_of_param(
    p: fa2_types__balance_of_param,
    s: fa2_types__Storage,
    params: Partial<ex.Parameters>,
  ): Promise<att.CallParameter> {
    if (this.address != undefined) {
      return await ex.get_call_param(
        this.address,
        "balance_of",
        balance_of_arg_to_mich(p, s),
        params,
      );
    }
    throw new Error("Contract not initialised");
  }
  async get_mint_param(
    sender: att.Address,
    p: fa2_types__mint_param,
    s: fa2_types__Storage,
    params: Partial<ex.Parameters>,
  ): Promise<att.CallParameter> {
    if (this.address != undefined) {
      return await ex.get_call_param(this.address, "mint", mint_arg_to_mich(sender, p, s), params);
    }
    throw new Error("Contract not initialised");
  }
  async get_default_param(
    p: fa2_types__proxy_execute_param,
    params: Partial<ex.Parameters>,
  ): Promise<att.CallParameter> {
    if (this.address != undefined) {
      return await ex.get_call_param(this.address, "default", default_arg_to_mich(p), params);
    }
    throw new Error("Contract not initialised");
  }
  async get_fa2(): Promise<att.Address> {
    if (this.address != undefined) {
      const storage = await ex.get_raw_storage(this.address);
      return att.Address.from_mich(storage);
    }
    throw new Error("Contract not initialised");
  }
  errors = {
    UNPACK_FAILED__INVALID_MINT_PARAMETERS: att.string_to_mich(
      '"UNPACK_FAILED: Invalid mint parameters"',
    ),
    UNPACK_FAILED__INVALID_BALANCE_OF_PARAMETERS: att.string_to_mich(
      '"UNPACK_FAILED: Invalid balance_of parameters"',
    ),
    UNPACK_FAILED__INVALID_UPDATE_OPERATORS_PARAMETERS: att.string_to_mich(
      '"UNPACK_FAILED: Invalid update_operators parameters"',
    ),
    UNPACK_FAILED__INVALID_TRANSFER_PARAMETERS: att.string_to_mich(
      '"UNPACK_FAILED: Invalid transfer parameters"',
    ),
    INVALID_CALLER: att.string_to_mich('"INVALID_CALLER"'),
    m_r2: att.pair_to_mich([
      att.string_to_mich('"INVALID_CONDITION"'),
      att.string_to_mich('"m_r2"'),
    ]),
    m_r1: att.string_to_mich('"FA2_NOT_OWNER"'),
    NO_TRANSFER: att.string_to_mich('"NO_TRANSFER"'),
    INVALID_CALLBACK_CONTRACT: att.string_to_mich('"Invalid callback contract"'),
    FA2_NOT_OWNER: att.string_to_mich('"FA2_NOT_OWNER"'),
    uo_r1: att.pair_to_mich([
      att.string_to_mich('"INVALID_CONDITION"'),
      att.string_to_mich('"uo_r1"'),
    ]),
    t_r1: att.pair_to_mich([
      att.string_to_mich('"INVALID_CONDITION"'),
      att.string_to_mich('"t_r1"'),
    ]),
    FA2_INSUFFICIENT_BALANCE: att.string_to_mich('"FA2_INSUFFICIENT_BALANCE"'),
    FA2_TOKEN_UNDEFINED: att.string_to_mich('"FA2_TOKEN_UNDEFINED"'),
    FA2_NOT_OPERATOR: att.string_to_mich('"FA2_NOT_OPERATOR"'),
    CONTRACT_PAUSED: att.string_to_mich('"CONTRACT_PAUSED"'),
  };
}
export const fa2_logic = new Fa2_logic();
