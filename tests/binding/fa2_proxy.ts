import * as ex from "@completium/experiment-ts";
import * as att from "@completium/archetype-ts-types";
export enum fa2_types__e_entrypoints_types {
  Transfer = "Transfer",
  Update_Operators = "Update_Operators",
  Balance_Of = "Balance_Of",
  Mint = "Mint",
}
export abstract class fa2_types__e_entrypoints extends att.Enum<fa2_types__e_entrypoints_types> {
  abstract to_mich(): att.Micheline;
  equals(v: fa2_types__e_entrypoints): boolean {
    return att.micheline_equals(this.to_mich(), v.to_mich());
  }
}
export class Transfer extends fa2_types__e_entrypoints {
  constructor() {
    super(fa2_types__e_entrypoints_types.Transfer);
  }
  to_mich() {
    return new att.Int(0).to_mich();
  }
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
}
export class Update_Operators extends fa2_types__e_entrypoints {
  constructor() {
    super(fa2_types__e_entrypoints_types.Update_Operators);
  }
  to_mich() {
    return new att.Int(1).to_mich();
  }
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
}
export class Balance_Of extends fa2_types__e_entrypoints {
  constructor() {
    super(fa2_types__e_entrypoints_types.Balance_Of);
  }
  to_mich() {
    return new att.Int(2).to_mich();
  }
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
}
export class Mint extends fa2_types__e_entrypoints {
  constructor() {
    super(fa2_types__e_entrypoints_types.Mint);
  }
  to_mich() {
    return new att.Int(3).to_mich();
  }
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
}
export enum fa2_types__update_op_types {
  add_operator = "add_operator",
  remove_operator = "remove_operator",
}
export abstract class fa2_types__update_op extends att.Enum<fa2_types__update_op_types> {
  abstract to_mich(): att.Micheline;
  equals(v: fa2_types__update_op): boolean {
    return att.micheline_equals(this.to_mich(), v.to_mich());
  }
}
export class add_operator extends fa2_types__update_op {
  constructor(private content: fa2_types__operator_param) {
    super(fa2_types__update_op_types.add_operator);
  }
  to_mich() {
    return att.left_to_mich(this.content.to_mich());
  }
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
  get() {
    return this.content;
  }
}
export class remove_operator extends fa2_types__update_op {
  constructor(private content: fa2_types__operator_param) {
    super(fa2_types__update_op_types.remove_operator);
  }
  to_mich() {
    return att.right_to_mich(this.content.to_mich());
  }
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
  get() {
    return this.content;
  }
}
export const mich_to_fa2_types__e_entrypoints = (m: any): fa2_types__e_entrypoints => {
  const v = new att.Nat((m as att.Mint).int).to_big_number().toNumber();
  switch (v) {
    case 0:
      return new Transfer();
    case 1:
      return new Update_Operators();
    case 2:
      return new Balance_Of();
    case 3:
      return new Mint();
    default:
      throw new Error("mich_to_asset_type : invalid value " + v);
  }
};
export const mich_to_fa2_types__update_op = (m: att.Micheline): fa2_types__update_op => {
  if ((m as att.Msingle).prim == "Left") {
    return new add_operator(fa2_types__operator_param.from_mich((m as att.Msingle).args[0]));
  }
  if ((m as att.Msingle).prim == "Right") {
    return new remove_operator(fa2_types__operator_param.from_mich((m as att.Msingle).args[0]));
  }
  throw new Error("mich_to_fa2_types__update_op : invalid micheline");
};
export class fa2_types__Storage implements att.ArchetypeType {
  constructor(
    public owner: att.Address,
    public permits: att.Address,
    public token_metadata: Array<[att.Nat, [att.Nat, Array<[att.Nat, att.Bytes]>]]>,
    public ledger: Array<[[att.Address, att.Nat], att.Nat]>,
    public operator: Array<[[att.Address, att.Nat, att.Address], att.Unit]>,
    public operator_for_all: Array<[[att.Address, att.Address], att.Unit]>,
    public paused: boolean,
  ) {}
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
  to_mich(): att.Micheline {
    return att.pair_to_mich([
      this.owner.to_mich(),
      this.permits.to_mich(),
      att.list_to_mich(this.token_metadata, x => {
        const x_key = x[0];
        const x_value = x[1];
        return att.elt_to_mich(
          x_key.to_mich(),
          att.pair_to_mich([
            x_value[0].to_mich(),
            att.list_to_mich(x_value[1], x => {
              const x_key = x[0];
              const x_value = x[1];
              return att.elt_to_mich(x_key.to_mich(), x_value.to_mich());
            }),
          ]),
        );
      }),
      att.list_to_mich(this.ledger, x => {
        const x_key = x[0];
        const x_value = x[1];
        return att.elt_to_mich(
          att.pair_to_mich([x_key[0].to_mich(), x_key[1].to_mich()]),
          x_value.to_mich(),
        );
      }),
      att.list_to_mich(this.operator, x => {
        const x_key = x[0];
        const x_value = x[1];
        return att.elt_to_mich(
          att.pair_to_mich([x_key[0].to_mich(), x_key[1].to_mich(), x_key[2].to_mich()]),
          att.unit_to_mich(),
        );
      }),
      att.list_to_mich(this.operator_for_all, x => {
        const x_key = x[0];
        const x_value = x[1];
        return att.elt_to_mich(
          att.pair_to_mich([x_key[0].to_mich(), x_key[1].to_mich()]),
          att.unit_to_mich(),
        );
      }),
      att.bool_to_mich(this.paused),
    ]);
  }
  equals(v: fa2_types__Storage): boolean {
    return att.micheline_equals(this.to_mich(), v.to_mich());
  }
  static from_mich(input: att.Micheline): fa2_types__Storage {
    return new fa2_types__Storage(
      att.Address.from_mich((input as att.Mpair).args[0]),
      att.Address.from_mich((input as att.Mpair).args[1]),
      att.Int.from_mich((input as att.Mpair).args[2]) as any,
      att.Int.from_mich((input as att.Mpair).args[3]) as any,
      att.Int.from_mich((input as att.Mpair).args[4]) as any,
      att.Int.from_mich((input as att.Mpair).args[5]) as any,
      att.mich_to_bool((input as att.Mpair).args[6]),
    );
  }
}
export class fa2_types__proxy_execute_param implements att.ArchetypeType {
  constructor(
    public pe_entrypoint: fa2_types__e_entrypoints,
    public pe_sender: att.Address,
    public pe_params: att.Bytes,
    public pe_storage: fa2_types__Storage,
  ) {}
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
  to_mich(): att.Micheline {
    return att.pair_to_mich([
      this.pe_entrypoint.to_mich(),
      this.pe_sender.to_mich(),
      this.pe_params.to_mich(),
      this.pe_storage.to_mich(),
    ]);
  }
  equals(v: fa2_types__proxy_execute_param): boolean {
    return att.micheline_equals(this.to_mich(), v.to_mich());
  }
  static from_mich(input: att.Micheline): fa2_types__proxy_execute_param {
    return new fa2_types__proxy_execute_param(
      mich_to_fa2_types__e_entrypoints((input as att.Mpair).args[0]),
      att.Address.from_mich((input as att.Mpair).args[1]),
      att.Bytes.from_mich((input as att.Mpair).args[2]),
      fa2_types__Storage.from_mich(
        att.pair_to_mich((input as att.Mpair as att.Mpair).args.slice(3, 10)),
      ),
    );
  }
}
export class fa2_types__transfer_destination implements att.ArchetypeType {
  constructor(
    public to_dest: att.Address,
    public token_id_dest: att.Nat,
    public token_amount_dest: att.Nat,
  ) {}
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
  to_mich(): att.Micheline {
    return att.pair_to_mich([
      this.to_dest.to_mich(),
      att.pair_to_mich([this.token_id_dest.to_mich(), this.token_amount_dest.to_mich()]),
    ]);
  }
  equals(v: fa2_types__transfer_destination): boolean {
    return att.micheline_equals(this.to_mich(), v.to_mich());
  }
  static from_mich(input: att.Micheline): fa2_types__transfer_destination {
    return new fa2_types__transfer_destination(
      att.Address.from_mich((input as att.Mpair).args[0]),
      att.Nat.from_mich(
        (att.pair_to_mich((input as att.Mpair as att.Mpair).args.slice(1, 3)) as att.Mpair).args[0],
      ),
      att.Nat.from_mich(
        (att.pair_to_mich((input as att.Mpair as att.Mpair).args.slice(1, 3)) as att.Mpair).args[1],
      ),
    );
  }
}
export class fa2_types__transfer_param implements att.ArchetypeType {
  constructor(
    public tp_from: att.Address,
    public tp_txs: Array<fa2_types__transfer_destination>,
  ) {}
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
  to_mich(): att.Micheline {
    return att.pair_to_mich([
      this.tp_from.to_mich(),
      att.list_to_mich(this.tp_txs, x => {
        return x.to_mich();
      }),
    ]);
  }
  equals(v: fa2_types__transfer_param): boolean {
    return att.micheline_equals(this.to_mich(), v.to_mich());
  }
  static from_mich(input: att.Micheline): fa2_types__transfer_param {
    return new fa2_types__transfer_param(
      att.Address.from_mich((input as att.Mpair).args[0]),
      att.mich_to_list((input as att.Mpair).args[1], x => {
        return fa2_types__transfer_destination.from_mich(x);
      }),
    );
  }
}
export class fa2_types__balance_of_request implements att.ArchetypeType {
  constructor(
    public bo_owner: att.Address,
    public btoken_id: att.Nat,
  ) {}
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
  to_mich(): att.Micheline {
    return att.pair_to_mich([this.bo_owner.to_mich(), this.btoken_id.to_mich()]);
  }
  equals(v: fa2_types__balance_of_request): boolean {
    return att.micheline_equals(this.to_mich(), v.to_mich());
  }
  static from_mich(input: att.Micheline): fa2_types__balance_of_request {
    return new fa2_types__balance_of_request(
      att.Address.from_mich((input as att.Mpair).args[0]),
      att.Nat.from_mich((input as att.Mpair).args[1]),
    );
  }
}
export class fa2_types__balance_of_response implements att.ArchetypeType {
  constructor(
    public request: fa2_types__balance_of_request,
    public balance_: att.Nat,
  ) {}
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
  to_mich(): att.Micheline {
    return att.pair_to_mich([this.request.to_mich(), this.balance_.to_mich()]);
  }
  equals(v: fa2_types__balance_of_response): boolean {
    return att.micheline_equals(this.to_mich(), v.to_mich());
  }
  static from_mich(input: att.Micheline): fa2_types__balance_of_response {
    return new fa2_types__balance_of_response(
      fa2_types__balance_of_request.from_mich((input as att.Mpair).args[0]),
      att.Nat.from_mich((input as att.Mpair).args[1]),
    );
  }
}
export class fa2_types__balance_of_param implements att.ArchetypeType {
  constructor(
    public requests: Array<fa2_types__balance_of_request>,
    public callback: att.Address,
  ) {}
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
  to_mich(): att.Micheline {
    return att.pair_to_mich([
      att.list_to_mich(this.requests, x => {
        return x.to_mich();
      }),
      this.callback.to_mich(),
    ]);
  }
  equals(v: fa2_types__balance_of_param): boolean {
    return att.micheline_equals(this.to_mich(), v.to_mich());
  }
  static from_mich(input: att.Micheline): fa2_types__balance_of_param {
    return new fa2_types__balance_of_param(
      att.mich_to_list((input as att.Mpair).args[0], x => {
        return fa2_types__balance_of_request.from_mich(x);
      }),
      att.Address.from_mich((input as att.Mpair).args[1]),
    );
  }
}
export class fa2_types__operator_param implements att.ArchetypeType {
  constructor(
    public opp_owner: att.Address,
    public opp_operator: att.Address,
    public opp_token_id: att.Nat,
  ) {}
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
  to_mich(): att.Micheline {
    return att.pair_to_mich([
      this.opp_owner.to_mich(),
      att.pair_to_mich([this.opp_operator.to_mich(), this.opp_token_id.to_mich()]),
    ]);
  }
  equals(v: fa2_types__operator_param): boolean {
    return att.micheline_equals(this.to_mich(), v.to_mich());
  }
  static from_mich(input: att.Micheline): fa2_types__operator_param {
    return new fa2_types__operator_param(
      att.Address.from_mich((input as att.Mpair).args[0]),
      att.Address.from_mich(
        (att.pair_to_mich((input as att.Mpair as att.Mpair).args.slice(1, 3)) as att.Mpair).args[0],
      ),
      att.Nat.from_mich(
        (att.pair_to_mich((input as att.Mpair as att.Mpair).args.slice(1, 3)) as att.Mpair).args[1],
      ),
    );
  }
}
export class fa2_types__mint_param implements att.ArchetypeType {
  constructor(
    public mp_to: att.Address,
    public mp_token_id: att.Nat,
    public mp_token_amount: att.Nat,
  ) {}
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
  to_mich(): att.Micheline {
    return att.pair_to_mich([
      this.mp_to.to_mich(),
      this.mp_token_id.to_mich(),
      this.mp_token_amount.to_mich(),
    ]);
  }
  equals(v: fa2_types__mint_param): boolean {
    return att.micheline_equals(this.to_mich(), v.to_mich());
  }
  static from_mich(input: att.Micheline): fa2_types__mint_param {
    return new fa2_types__mint_param(
      att.Address.from_mich((input as att.Mpair).args[0]),
      att.Nat.from_mich((input as att.Mpair).args[1]),
      att.Nat.from_mich((input as att.Mpair).args[2]),
    );
  }
}
export class exec_param implements att.ArchetypeType {
  constructor(
    public ep_entrypoint: fa2_types__e_entrypoints,
    public ep_params: att.Bytes,
  ) {}
  toString(): string {
    return JSON.stringify(this, null, 2);
  }
  to_mich(): att.Micheline {
    return att.pair_to_mich([this.ep_entrypoint.to_mich(), this.ep_params.to_mich()]);
  }
  equals(v: exec_param): boolean {
    return att.micheline_equals(this.to_mich(), v.to_mich());
  }
  static from_mich(input: att.Micheline): exec_param {
    return new exec_param(
      mich_to_fa2_types__e_entrypoints((input as att.Mpair).args[0]),
      att.Bytes.from_mich((input as att.Mpair).args[1]),
    );
  }
}
export const fa2_types__Storage_mich_type: att.MichelineType = att.pair_array_to_mich_type(
  [
    att.prim_annot_to_mich_type("address", ["%owner"]),
    att.prim_annot_to_mich_type("address", ["%permits"]),
    att.pair_annot_to_mich_type(
      "big_map",
      att.prim_annot_to_mich_type("nat", []),
      att.pair_array_to_mich_type(
        [
          att.prim_annot_to_mich_type("nat", []),
          att.pair_annot_to_mich_type(
            "map",
            att.prim_annot_to_mich_type("nat", []),
            att.prim_annot_to_mich_type("bytes", []),
            [],
          ),
        ],
        [],
      ),
      ["%token_metadata"],
    ),
    att.pair_annot_to_mich_type(
      "big_map",
      att.pair_array_to_mich_type(
        [att.prim_annot_to_mich_type("address", []), att.prim_annot_to_mich_type("nat", [])],
        [],
      ),
      att.prim_annot_to_mich_type("nat", []),
      ["%ledger"],
    ),
    att.pair_annot_to_mich_type(
      "big_map",
      att.pair_array_to_mich_type(
        [
          att.prim_annot_to_mich_type("address", []),
          att.prim_annot_to_mich_type("nat", []),
          att.prim_annot_to_mich_type("address", []),
        ],
        [],
      ),
      att.prim_annot_to_mich_type("unit", []),
      ["%operator"],
    ),
    att.pair_annot_to_mich_type(
      "big_map",
      att.pair_array_to_mich_type(
        [att.prim_annot_to_mich_type("address", []), att.prim_annot_to_mich_type("address", [])],
        [],
      ),
      att.prim_annot_to_mich_type("unit", []),
      ["%operator_for_all"],
    ),
    att.prim_annot_to_mich_type("bool", ["%paused"]),
  ],
  [],
);
export const fa2_types__proxy_execute_param_mich_type: att.MichelineType =
  att.pair_array_to_mich_type(
    [
      att.prim_annot_to_mich_type("int", ["%pe_entrypoint"]),
      att.prim_annot_to_mich_type("address", ["%pe_sender"]),
      att.prim_annot_to_mich_type("bytes", ["%pe_params"]),
      att.pair_array_to_mich_type(
        [
          att.prim_annot_to_mich_type("address", ["%owner"]),
          att.prim_annot_to_mich_type("address", ["%permits"]),
          att.pair_annot_to_mich_type(
            "big_map",
            att.prim_annot_to_mich_type("nat", []),
            att.pair_array_to_mich_type(
              [
                att.prim_annot_to_mich_type("nat", []),
                att.pair_annot_to_mich_type(
                  "map",
                  att.prim_annot_to_mich_type("nat", []),
                  att.prim_annot_to_mich_type("bytes", []),
                  [],
                ),
              ],
              [],
            ),
            ["%token_metadata"],
          ),
          att.pair_annot_to_mich_type(
            "big_map",
            att.pair_array_to_mich_type(
              [att.prim_annot_to_mich_type("address", []), att.prim_annot_to_mich_type("nat", [])],
              [],
            ),
            att.prim_annot_to_mich_type("nat", []),
            ["%ledger"],
          ),
          att.pair_annot_to_mich_type(
            "big_map",
            att.pair_array_to_mich_type(
              [
                att.prim_annot_to_mich_type("address", []),
                att.prim_annot_to_mich_type("nat", []),
                att.prim_annot_to_mich_type("address", []),
              ],
              [],
            ),
            att.prim_annot_to_mich_type("unit", []),
            ["%operator"],
          ),
          att.pair_annot_to_mich_type(
            "big_map",
            att.pair_array_to_mich_type(
              [
                att.prim_annot_to_mich_type("address", []),
                att.prim_annot_to_mich_type("address", []),
              ],
              [],
            ),
            att.prim_annot_to_mich_type("unit", []),
            ["%operator_for_all"],
          ),
          att.prim_annot_to_mich_type("bool", ["%paused"]),
        ],
        ["%pe_storage"],
      ),
    ],
    [],
  );
export const fa2_types__transfer_destination_mich_type: att.MichelineType =
  att.pair_array_to_mich_type(
    [
      att.prim_annot_to_mich_type("address", ["%to_"]),
      att.pair_array_to_mich_type(
        [
          att.prim_annot_to_mich_type("nat", ["%token_id"]),
          att.prim_annot_to_mich_type("nat", ["%amount"]),
        ],
        [],
      ),
    ],
    [],
  );
export const fa2_types__transfer_param_mich_type: att.MichelineType = att.pair_array_to_mich_type(
  [
    att.prim_annot_to_mich_type("address", ["%from_"]),
    att.list_annot_to_mich_type(
      att.pair_array_to_mich_type(
        [
          att.prim_annot_to_mich_type("address", ["%to_"]),
          att.pair_array_to_mich_type(
            [
              att.prim_annot_to_mich_type("nat", ["%token_id"]),
              att.prim_annot_to_mich_type("nat", ["%amount"]),
            ],
            [],
          ),
        ],
        [],
      ),
      ["%txs"],
    ),
  ],
  [],
);
export const fa2_types__balance_of_request_mich_type: att.MichelineType =
  att.pair_array_to_mich_type(
    [
      att.prim_annot_to_mich_type("address", ["%owner"]),
      att.prim_annot_to_mich_type("nat", ["%token_id"]),
    ],
    [],
  );
export const fa2_types__balance_of_response_mich_type: att.MichelineType =
  att.pair_array_to_mich_type(
    [
      att.pair_array_to_mich_type(
        [
          att.prim_annot_to_mich_type("address", ["%owner"]),
          att.prim_annot_to_mich_type("nat", ["%token_id"]),
        ],
        ["%request"],
      ),
      att.prim_annot_to_mich_type("nat", ["%balance"]),
    ],
    [],
  );
export const fa2_types__balance_of_param_mich_type: att.MichelineType = att.pair_array_to_mich_type(
  [
    att.list_annot_to_mich_type(
      att.pair_array_to_mich_type(
        [
          att.prim_annot_to_mich_type("address", ["%owner"]),
          att.prim_annot_to_mich_type("nat", ["%token_id"]),
        ],
        [],
      ),
      ["%request"],
    ),
    att.prim_annot_to_mich_type("address", ["%callback"]),
  ],
  [],
);
export const fa2_types__operator_param_mich_type: att.MichelineType = att.pair_array_to_mich_type(
  [
    att.prim_annot_to_mich_type("address", ["%owner"]),
    att.pair_array_to_mich_type(
      [
        att.prim_annot_to_mich_type("address", ["%operator"]),
        att.prim_annot_to_mich_type("nat", ["%token_id"]),
      ],
      [],
    ),
  ],
  [],
);
export const fa2_types__mint_param_mich_type: att.MichelineType = att.pair_array_to_mich_type(
  [
    att.prim_annot_to_mich_type("address", ["%mp_to"]),
    att.prim_annot_to_mich_type("nat", ["%mp_token_id"]),
    att.prim_annot_to_mich_type("nat", ["%mp_token_amount"]),
  ],
  [],
);
export const exec_param_mich_type: att.MichelineType = att.pair_array_to_mich_type(
  [
    att.prim_annot_to_mich_type("int", ["%ep_entrypoint"]),
    att.prim_annot_to_mich_type("bytes", ["%ep_params"]),
  ],
  [],
);
const set_proxy_address_arg_to_mich = (candidate: att.Address): att.Micheline => {
  return candidate.to_mich();
};
const exec_arg_to_mich = (p: exec_param): att.Micheline => {
  return p.to_mich();
};
const apply_storage_arg_to_mich = (p: fa2_types__Storage): att.Micheline => {
  return p.to_mich();
};
const update_operators_arg_to_mich = (upl: Array<fa2_types__update_op>): att.Micheline => {
  return att.list_to_mich(upl, x => {
    return x.to_mich();
  });
};
const transfer_arg_to_mich = (txs: Array<fa2_types__transfer_param>): att.Micheline => {
  return att.list_to_mich(txs, x => {
    return x.to_mich();
  });
};
const balance_of_arg_to_mich = (
  requests: Array<fa2_types__balance_of_request>,
  callback: att.Entrypoint,
): att.Micheline => {
  return att.pair_to_mich([
    att.list_to_mich(requests, x => {
      return x.to_mich();
    }),
    callback.to_mich(),
  ]);
};
export class Fa2_proxy {
  address: string | undefined;
  constructor(address: string | undefined = undefined) {
    this.address = address;
  }
  get_address(): att.Address {
    if (undefined != this.address) {
      return new att.Address(this.address);
    }
    throw new Error("Contract not initialised");
  }
  async get_balance(): Promise<att.Tez> {
    if (null != this.address) {
      return await ex.get_balance(new att.Address(this.address));
    }
    throw new Error("Contract not initialised");
  }
  async deploy(owner: att.Address, permits: att.Address, params: Partial<ex.Parameters>) {
    const address = (
      await ex.deploy(
        "./contracts/fa2_proxy.arl",
        {
          owner: owner.to_mich(),
          permits: permits.to_mich(),
        },
        params,
      )
    ).address;
    this.address = address;
  }
  async set_proxy_address(
    candidate: att.Address,
    params: Partial<ex.Parameters>,
  ): Promise<att.CallResult> {
    if (this.address != undefined) {
      return await ex.call(
        this.address,
        "set_proxy_address",
        set_proxy_address_arg_to_mich(candidate),
        params,
      );
    }
    throw new Error("Contract not initialised");
  }
  async exec(p: exec_param, params: Partial<ex.Parameters>): Promise<att.CallResult> {
    if (this.address != undefined) {
      return await ex.call(this.address, "exec", exec_arg_to_mich(p), params);
    }
    throw new Error("Contract not initialised");
  }
  async apply_storage(
    p: fa2_types__Storage,
    params: Partial<ex.Parameters>,
  ): Promise<att.CallResult> {
    if (this.address != undefined) {
      return await ex.call(this.address, "apply_storage", apply_storage_arg_to_mich(p), params);
    }
    throw new Error("Contract not initialised");
  }
  async update_operators(
    upl: Array<fa2_types__update_op>,
    params: Partial<ex.Parameters>,
  ): Promise<att.CallResult> {
    if (this.address != undefined) {
      return await ex.call(
        this.address,
        "update_operators",
        update_operators_arg_to_mich(upl),
        params,
      );
    }
    throw new Error("Contract not initialised");
  }
  async transfer(
    txs: Array<fa2_types__transfer_param>,
    params: Partial<ex.Parameters>,
  ): Promise<att.CallResult> {
    if (this.address != undefined) {
      return await ex.call(this.address, "transfer", transfer_arg_to_mich(txs), params);
    }
    throw new Error("Contract not initialised");
  }
  async balance_of(
    requests: Array<fa2_types__balance_of_request>,
    callback: att.Entrypoint,
    params: Partial<ex.Parameters>,
  ): Promise<att.CallResult> {
    if (this.address != undefined) {
      return await ex.call(
        this.address,
        "balance_of",
        balance_of_arg_to_mich(requests, callback),
        params,
      );
    }
    throw new Error("Contract not initialised");
  }
  async get_set_proxy_address_param(
    candidate: att.Address,
    params: Partial<ex.Parameters>,
  ): Promise<att.CallParameter> {
    if (this.address != undefined) {
      return await ex.get_call_param(
        this.address,
        "set_proxy_address",
        set_proxy_address_arg_to_mich(candidate),
        params,
      );
    }
    throw new Error("Contract not initialised");
  }
  async get_exec_param(p: exec_param, params: Partial<ex.Parameters>): Promise<att.CallParameter> {
    if (this.address != undefined) {
      return await ex.get_call_param(this.address, "exec", exec_arg_to_mich(p), params);
    }
    throw new Error("Contract not initialised");
  }
  async get_apply_storage_param(
    p: fa2_types__Storage,
    params: Partial<ex.Parameters>,
  ): Promise<att.CallParameter> {
    if (this.address != undefined) {
      return await ex.get_call_param(
        this.address,
        "apply_storage",
        apply_storage_arg_to_mich(p),
        params,
      );
    }
    throw new Error("Contract not initialised");
  }
  async get_update_operators_param(
    upl: Array<fa2_types__update_op>,
    params: Partial<ex.Parameters>,
  ): Promise<att.CallParameter> {
    if (this.address != undefined) {
      return await ex.get_call_param(
        this.address,
        "update_operators",
        update_operators_arg_to_mich(upl),
        params,
      );
    }
    throw new Error("Contract not initialised");
  }
  async get_transfer_param(
    txs: Array<fa2_types__transfer_param>,
    params: Partial<ex.Parameters>,
  ): Promise<att.CallParameter> {
    if (this.address != undefined) {
      return await ex.get_call_param(this.address, "transfer", transfer_arg_to_mich(txs), params);
    }
    throw new Error("Contract not initialised");
  }
  async get_balance_of_param(
    requests: Array<fa2_types__balance_of_request>,
    callback: att.Entrypoint,
    params: Partial<ex.Parameters>,
  ): Promise<att.CallParameter> {
    if (this.address != undefined) {
      return await ex.get_call_param(
        this.address,
        "balance_of",
        balance_of_arg_to_mich(requests, callback),
        params,
      );
    }
    throw new Error("Contract not initialised");
  }
  async get_owner(): Promise<att.Address> {
    if (this.address != undefined) {
      const storage = await ex.get_raw_storage(this.address);
      return att.Address.from_mich((storage as att.Mpair).args[0]);
    }
    throw new Error("Contract not initialised");
  }
  async get_permits(): Promise<att.Address> {
    if (this.address != undefined) {
      const storage = await ex.get_raw_storage(this.address);
      return att.Address.from_mich((storage as att.Mpair).args[1]);
    }
    throw new Error("Contract not initialised");
  }
  async get_token_metadata_value(
    key: att.Nat,
  ): Promise<[att.Nat, Array<[att.Nat, att.Bytes]>] | undefined> {
    if (this.address != undefined) {
      const storage = await ex.get_raw_storage(this.address);
      const data = await ex.get_big_map_value(
        BigInt(att.Int.from_mich((storage as att.Mpair).args[2]).toString()),
        key.to_mich(),
        att.prim_annot_to_mich_type("nat", []),
      );
      if (data != undefined) {
        return (p => {
          return [
            att.Nat.from_mich((p as att.Mpair).args[0]),
            att.mich_to_map((p as att.Mpair).args[1], (x, y) => [
              att.Nat.from_mich(x),
              att.Bytes.from_mich(y),
            ]),
          ];
        })(data);
      } else {
        return undefined;
      }
    }
    throw new Error("Contract not initialised");
  }
  async has_token_metadata_value(key: att.Nat): Promise<boolean> {
    if (this.address != undefined) {
      const storage = await ex.get_raw_storage(this.address);
      const data = await ex.get_big_map_value(
        BigInt(att.Int.from_mich((storage as att.Mpair).args[2]).toString()),
        key.to_mich(),
        att.prim_annot_to_mich_type("nat", []),
      );
      if (data != undefined) {
        return true;
      } else {
        return false;
      }
    }
    throw new Error("Contract not initialised");
  }
  async get_ledger_value(key: [att.Address, att.Nat]): Promise<att.Nat | undefined> {
    if (this.address != undefined) {
      const storage = await ex.get_raw_storage(this.address);
      const data = await ex.get_big_map_value(
        BigInt(att.Int.from_mich((storage as att.Mpair).args[3]).toString()),
        att.pair_to_mich([key[0].to_mich(), key[1].to_mich()]),
        att.pair_array_to_mich_type(
          [att.prim_annot_to_mich_type("address", []), att.prim_annot_to_mich_type("nat", [])],
          [],
        ),
      );
      if (data != undefined) {
        return att.Nat.from_mich(data);
      } else {
        return undefined;
      }
    }
    throw new Error("Contract not initialised");
  }
  async has_ledger_value(key: [att.Address, att.Nat]): Promise<boolean> {
    if (this.address != undefined) {
      const storage = await ex.get_raw_storage(this.address);
      const data = await ex.get_big_map_value(
        BigInt(att.Int.from_mich((storage as att.Mpair).args[3]).toString()),
        att.pair_to_mich([key[0].to_mich(), key[1].to_mich()]),
        att.pair_array_to_mich_type(
          [att.prim_annot_to_mich_type("address", []), att.prim_annot_to_mich_type("nat", [])],
          [],
        ),
      );
      if (data != undefined) {
        return true;
      } else {
        return false;
      }
    }
    throw new Error("Contract not initialised");
  }
  async get_operator_value(
    key: [att.Address, att.Nat, att.Address],
  ): Promise<att.Unit | undefined> {
    if (this.address != undefined) {
      const storage = await ex.get_raw_storage(this.address);
      const data = await ex.get_big_map_value(
        BigInt(att.Int.from_mich((storage as att.Mpair).args[4]).toString()),
        att.pair_to_mich([key[0].to_mich(), key[1].to_mich(), key[2].to_mich()]),
        att.pair_array_to_mich_type(
          [
            att.prim_annot_to_mich_type("address", []),
            att.prim_annot_to_mich_type("nat", []),
            att.prim_annot_to_mich_type("address", []),
          ],
          [],
        ),
      );
      if (data != undefined) {
        return new att.Unit();
      } else {
        return undefined;
      }
    }
    throw new Error("Contract not initialised");
  }
  async has_operator_value(key: [att.Address, att.Nat, att.Address]): Promise<boolean> {
    if (this.address != undefined) {
      const storage = await ex.get_raw_storage(this.address);
      const data = await ex.get_big_map_value(
        BigInt(att.Int.from_mich((storage as att.Mpair).args[4]).toString()),
        att.pair_to_mich([key[0].to_mich(), key[1].to_mich(), key[2].to_mich()]),
        att.pair_array_to_mich_type(
          [
            att.prim_annot_to_mich_type("address", []),
            att.prim_annot_to_mich_type("nat", []),
            att.prim_annot_to_mich_type("address", []),
          ],
          [],
        ),
      );
      if (data != undefined) {
        return true;
      } else {
        return false;
      }
    }
    throw new Error("Contract not initialised");
  }
  async get_operator_for_all_value(key: [att.Address, att.Address]): Promise<att.Unit | undefined> {
    if (this.address != undefined) {
      const storage = await ex.get_raw_storage(this.address);
      const data = await ex.get_big_map_value(
        BigInt(att.Int.from_mich((storage as att.Mpair).args[5]).toString()),
        att.pair_to_mich([key[0].to_mich(), key[1].to_mich()]),
        att.pair_array_to_mich_type(
          [att.prim_annot_to_mich_type("address", []), att.prim_annot_to_mich_type("address", [])],
          [],
        ),
      );
      if (data != undefined) {
        return new att.Unit();
      } else {
        return undefined;
      }
    }
    throw new Error("Contract not initialised");
  }
  async has_operator_for_all_value(key: [att.Address, att.Address]): Promise<boolean> {
    if (this.address != undefined) {
      const storage = await ex.get_raw_storage(this.address);
      const data = await ex.get_big_map_value(
        BigInt(att.Int.from_mich((storage as att.Mpair).args[5]).toString()),
        att.pair_to_mich([key[0].to_mich(), key[1].to_mich()]),
        att.pair_array_to_mich_type(
          [att.prim_annot_to_mich_type("address", []), att.prim_annot_to_mich_type("address", [])],
          [],
        ),
      );
      if (data != undefined) {
        return true;
      } else {
        return false;
      }
    }
    throw new Error("Contract not initialised");
  }
  async get_paused(): Promise<boolean> {
    if (this.address != undefined) {
      const storage = await ex.get_raw_storage(this.address);
      return att.mich_to_bool((storage as att.Mpair).args[6]);
    }
    throw new Error("Contract not initialised");
  }
  async get_proxy_address(): Promise<att.Address> {
    if (this.address != undefined) {
      const storage = await ex.get_raw_storage(this.address);
      return att.Address.from_mich((storage as att.Mpair).args[7]);
    }
    throw new Error("Contract not initialised");
  }
  async get_metadata_value(key: string): Promise<att.Bytes | undefined> {
    if (this.address != undefined) {
      const storage = await ex.get_raw_storage(this.address);
      const data = await ex.get_big_map_value(
        BigInt(att.Int.from_mich((storage as att.Mpair).args[8]).toString()),
        att.string_to_mich(key),
        att.prim_annot_to_mich_type("string", []),
      );
      if (data != undefined) {
        return att.Bytes.from_mich(data);
      } else {
        return undefined;
      }
    }
    throw new Error("Contract not initialised");
  }
  async has_metadata_value(key: string): Promise<boolean> {
    if (this.address != undefined) {
      const storage = await ex.get_raw_storage(this.address);
      const data = await ex.get_big_map_value(
        BigInt(att.Int.from_mich((storage as att.Mpair).args[8]).toString()),
        att.string_to_mich(key),
        att.prim_annot_to_mich_type("string", []),
      );
      if (data != undefined) {
        return true;
      } else {
        return false;
      }
    }
    throw new Error("Contract not initialised");
  }
  errors = {
    NO_TRANSFER: att.string_to_mich('"NO_TRANSFER"'),
    INVALID_CALLER: att.string_to_mich('"INVALID_CALLER"'),
  };
}
export const fa2_proxy = new Fa2_proxy();
