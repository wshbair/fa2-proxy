import { Nat } from "@completium/archetype-ts-types";
import {
  expect_to_fail,
  get_account,
  pack,
  set_mockup,
  set_quiet,
} from "@completium/experiment-ts";
import assert from "assert";

import { fa2_proxy, Mint, exec_param } from "./binding/fa2_proxy";
import { fa2_logic } from "./binding/fa2_logic";
import { add, permits } from "./binding/permits";
import {
  mint_param,
  mint_param_mich_type,
  transfer_destination,
  transfer_param,
} from "./binding/fa2_types";

const alice = get_account("alice"); /* Account */
const bob = get_account("bob"); /* Account */
set_mockup(); /* Endpoint */
set_quiet(true); /* Verbose mode */
const amount = new Nat(1000); /* Amount */
const token_id = new Nat(0); /* Token ID */

before(async () => {
  await permits.deploy(alice.get_address(), { as: alice });
  await fa2_proxy.deploy(alice.get_address(), permits.get_address(), {
    as: alice,
  });
  await fa2_logic.deploy(fa2_proxy.get_address(), { as: alice });

  await permits.manage_consumer(new add(fa2_logic.get_address()), { as: alice });
  await fa2_proxy.set_proxy_address(fa2_logic.get_address(), { as: alice });
  await fa2_proxy.exec(
    new exec_param(
      new Mint(),
      pack(new mint_param(alice.get_address(), token_id, amount).to_mich(), mint_param_mich_type),
    ),
    { as: alice },
  );
});

describe("[FA2 Proxy - Multi-Asset] Transfers", async () => {
  it("Transfer simple amount of token", async () => {
    const balance_before_user1 = await fa2_proxy.get_ledger_value([alice.get_address(), token_id]);
    const balance_before_user2 = await fa2_proxy.get_ledger_value([bob.get_address(), token_id]);
    assert(balance_before_user1?.equals(new Nat(1000)), "Invalid amount before user1");
    assert(balance_before_user2 == undefined, "Invalid amount before user2");

    await fa2_proxy.transfer(
      [
        new transfer_param(alice.get_address(), [
          new transfer_destination(bob.get_address(), token_id, new Nat(1)),
        ]),
      ],
      { as: alice },
    );

    const balance_after_user1 = await fa2_proxy.get_ledger_value([alice.get_address(), token_id]);
    const balance_after_user2 = await fa2_proxy.get_ledger_value([bob.get_address(), token_id]);
    assert(balance_after_user1?.equals(new Nat(999)), "Invalid amount after user1");
    assert(balance_after_user2?.equals(new Nat(1)), "Invalid amount after user2");
  });

  it("Transfer a token from another user without a permit or an operator should fail", async () => {
    const balance_before_user1 = await fa2_proxy.get_ledger_value([alice.get_address(), token_id]);
    const balance_before_user2 = await fa2_proxy.get_ledger_value([bob.get_address(), token_id]);
    assert(balance_before_user1?.equals(new Nat(999)), "Invalid amount before user1");
    assert(balance_before_user2?.equals(new Nat(1)), "Invalid amount before user2");

    await expect_to_fail(async () => {
      await fa2_proxy.transfer(
        [
          new transfer_param(bob.get_address(), [
            new transfer_destination(alice.get_address(), token_id, new Nat(1)),
          ]),
        ],
        { as: alice },
      );
    }, fa2_logic.errors.FA2_NOT_OPERATOR);

    const balance_after_user1 = await fa2_proxy.get_ledger_value([alice.get_address(), token_id]);
    const balance_after_user2 = await fa2_proxy.get_ledger_value([bob.get_address(), token_id]);
    assert(balance_after_user1?.equals(new Nat(999)), "Invalid amount after user1");
    assert(balance_after_user2?.equals(new Nat(1)), "Invalid amount after user2");
  });

  it("Transfer more tokens than owned should fail", async () => {
    const balance_before_user1 = await fa2_proxy.get_ledger_value([alice.get_address(), token_id]);
    const balance_before_user2 = await fa2_proxy.get_ledger_value([bob.get_address(), token_id]);
    assert(balance_before_user1?.equals(new Nat(999)), "Invalid amount user1");
    assert(balance_before_user2?.equals(new Nat(1)), "Invalid amount user2");

    await expect_to_fail(async () => {
      await fa2_proxy.transfer(
        [
          new transfer_param(alice.get_address(), [
            new transfer_destination(bob.get_address(), token_id, new Nat(1000)),
          ]),
        ],
        { as: alice },
      );
    }, fa2_logic.errors.FA2_INSUFFICIENT_BALANCE);

    const balance_after_user1 = await fa2_proxy.get_ledger_value([alice.get_address(), token_id]);
    const balance_after_user2 = await fa2_proxy.get_ledger_value([bob.get_address(), token_id]);
    assert(balance_after_user1?.equals(new Nat(999)), "Invalid amount after user1");
    assert(balance_after_user2?.equals(new Nat(1)), "Invalid amount after user2");
  });
});
