import {
  ContractAbstractionFromContractType,
  WalletContractAbstractionFromContractType,
} from "./type-utils";
import { address, BigMap, bytes, contract, int, MMap, nat, unit } from "./type-aliases";

export type Storage = {
  owner: address;
  permits: address;
  token_metadata: BigMap<
    nat,
    {
      0: nat;
      1: MMap<nat, bytes>;
    }
  >;
  ledger: BigMap<
    {
      0: address;
      1: nat;
    },
    nat
  >;
  operator: BigMap<
    {
      0: address;
      1: nat;
      2: address;
    },
    unit
  >;
  operator_for_all: BigMap<
    {
      0: address;
      1: address;
    },
    unit
  >;
  paused: boolean;
  proxy_address: address;
  metadata: BigMap<string, bytes>;
};

type Methods = {
  set_proxy_address: (param: address) => Promise<void>;
  exec: (ep_entrypoint: int, ep_params: bytes) => Promise<void>;
  apply_storage: (
    owner: address,
    permits: address,
    token_metadata: BigMap<
      nat,
      {
        0: nat;
        1: MMap<nat, bytes>;
      }
    >,
    ledger: BigMap<
      {
        0: address;
        1: nat;
      },
      nat
    >,
    operator: BigMap<
      {
        0: address;
        1: nat;
        2: address;
      },
      unit
    >,
    operator_for_all: BigMap<
      {
        0: address;
        1: address;
      },
      unit
    >,
    paused: boolean,
  ) => Promise<void>;
  add_operator: (owner: address, operator: address, token_id: nat) => Promise<void>;
  remove_operator: (owner: address, operator: address, token_id: nat) => Promise<void>;
  transfer: (
    param: Array<{
      from_: address;
      txs: Array<{
        to_: address;
        token_id: nat;
        amount: nat;
      }>;
    }>,
  ) => Promise<void>;
  balance_of: (
    requests: Array<{
      owner: address;
      token_id: nat;
    }>,
    callback: contract,
  ) => Promise<void>;
};

type MethodsObject = {
  set_proxy_address: (param: address) => Promise<void>;
  exec: (params: { ep_entrypoint: int; ep_params: bytes }) => Promise<void>;
  apply_storage: (params: {
    owner: address;
    permits: address;
    token_metadata: BigMap<
      nat,
      {
        0: nat;
        1: MMap<nat, bytes>;
      }
    >;
    ledger: BigMap<
      {
        0: address;
        1: nat;
      },
      nat
    >;
    operator: BigMap<
      {
        0: address;
        1: nat;
        2: address;
      },
      unit
    >;
    operator_for_all: BigMap<
      {
        0: address;
        1: address;
      },
      unit
    >;
    paused: boolean;
  }) => Promise<void>;
  add_operator: (params: { owner: address; operator: address; token_id: nat }) => Promise<void>;
  remove_operator: (params: { owner: address; operator: address; token_id: nat }) => Promise<void>;
  transfer: (
    param: Array<{
      from_: address;
      txs: Array<{
        to_: address;
        token_id: nat;
        amount: nat;
      }>;
    }>,
  ) => Promise<void>;
  balance_of: (params: {
    requests: Array<{
      owner: address;
      token_id: nat;
    }>;
    callback: contract;
  }) => Promise<void>;
};

type contractTypes = {
  methods: Methods;
  methodsObject: MethodsObject;
  storage: Storage;
  code: { __type: "Fa2ProxyCode"; protocol: string; code: object[] };
};
export type Fa2ProxyContractType = ContractAbstractionFromContractType<contractTypes>;
export type Fa2ProxyWalletType = WalletContractAbstractionFromContractType<contractTypes>;
