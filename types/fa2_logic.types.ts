import {
  ContractAbstractionFromContractType,
  WalletContractAbstractionFromContractType,
} from "./type-utils";
import { address, BigMap, bytes, int, MMap, nat, unit } from "./type-aliases";

export type Storage = address;

export type Methods = {
  do_transfer: (
    txs: Array<{
      from_: address;
      txs: Array<{
        to_: address;
        token_id: nat;
        amount: nat;
      }>;
    }>,
    owner: address,
    permits: address,
    token_metadata: BigMap<
      nat,
      {
        0: nat;
        1: MMap<nat, bytes>;
      }
    >,
    ledger: BigMap<
      {
        0: address;
        1: nat;
      },
      nat
    >,
    operator: BigMap<
      {
        0: address;
        1: nat;
        2: address;
      },
      unit
    >,
    operator_for_all: BigMap<
      {
        0: address;
        1: address;
      },
      unit
    >,
    paused: boolean,
  ) => Promise<void>;
  transfer: (
    sender: address,
    p: Array<{
      from_: address;
      txs: Array<{
        to_: address;
        token_id: nat;
        amount: nat;
      }>;
    }>,
    s: {
      owner: address;
      permits: address;
      token_metadata: BigMap<
        nat,
        {
          0: nat;
          1: MMap<nat, bytes>;
        }
      >;
      ledger: BigMap<
        {
          0: address;
          1: nat;
        },
        nat
      >;
      operator: BigMap<
        {
          0: address;
          1: nat;
          2: address;
        },
        unit
      >;
      operator_for_all: BigMap<
        {
          0: address;
          1: address;
        },
        unit
      >;
      paused: boolean;
    },
  ) => Promise<void>;
  update_operators: (
    sender: address,
    p: Array<
      | {
          add_operator: {
            owner: address;
            operator: address;
            token_id: nat;
          };
        }
      | {
          remove_operator: {
            owner: address;
            operator: address;
            token_id: nat;
          };
        }
    >,
    s: {
      owner: address;
      permits: address;
      token_metadata: BigMap<
        nat,
        {
          0: nat;
          1: MMap<nat, bytes>;
        }
      >;
      ledger: BigMap<
        {
          0: address;
          1: nat;
        },
        nat
      >;
      operator: BigMap<
        {
          0: address;
          1: nat;
          2: address;
        },
        unit
      >;
      operator_for_all: BigMap<
        {
          0: address;
          1: address;
        },
        unit
      >;
      paused: boolean;
    },
  ) => Promise<void>;
  balance_of: (
    request: Array<{
      owner: address;
      token_id: nat;
    }>,
    callback: address,
    owner: address,
    permits: address,
    token_metadata: BigMap<
      nat,
      {
        0: nat;
        1: MMap<nat, bytes>;
      }
    >,
    ledger: BigMap<
      {
        0: address;
        1: nat;
      },
      nat
    >,
    operator: BigMap<
      {
        0: address;
        1: nat;
        2: address;
      },
      unit
    >,
    operator_for_all: BigMap<
      {
        0: address;
        1: address;
      },
      unit
    >,
    paused: boolean,
  ) => Promise<void>;
  mint: (
    sender: address,
    p: {
      mp_to: address;
      mp_token_id: nat;
      mp_token_amount: nat;
    },
    s: {
      owner: address;
      permits: address;
      token_metadata: BigMap<
        nat,
        {
          0: nat;
          1: MMap<nat, bytes>;
        }
      >;
      ledger: BigMap<
        {
          0: address;
          1: nat;
        },
        nat
      >;
      operator: BigMap<
        {
          0: address;
          1: nat;
          2: address;
        },
        unit
      >;
      operator_for_all: BigMap<
        {
          0: address;
          1: address;
        },
        unit
      >;
      paused: boolean;
    },
  ) => Promise<void>;
  default: (
    pe_entrypoint: int,
    pe_sender: address,
    pe_params: bytes,
    owner: address,
    permits: address,
    token_metadata: BigMap<
      nat,
      {
        0: nat;
        1: MMap<nat, bytes>;
      }
    >,
    ledger: BigMap<
      {
        0: address;
        1: nat;
      },
      nat
    >,
    operator: BigMap<
      {
        0: address;
        1: nat;
        2: address;
      },
      unit
    >,
    operator_for_all: BigMap<
      {
        0: address;
        1: address;
      },
      unit
    >,
    paused: boolean,
  ) => Promise<void>;
};

type MethodsObject = {
  do_transfer: (params: {
    txs: Array<{
      from_: address;
      txs: Array<{
        to_: address;
        token_id: nat;
        amount: nat;
      }>;
    }>;
    owner: address;
    permits: address;
    token_metadata: BigMap<
      nat,
      {
        0: nat;
        1: MMap<nat, bytes>;
      }
    >;
    ledger: BigMap<
      {
        0: address;
        1: nat;
      },
      nat
    >;
    operator: BigMap<
      {
        0: address;
        1: nat;
        2: address;
      },
      unit
    >;
    operator_for_all: BigMap<
      {
        0: address;
        1: address;
      },
      unit
    >;
    paused: boolean;
  }) => Promise<void>;
  transfer: (params: {
    sender: address;
    p: Array<{
      from_: address;
      txs: Array<{
        to_: address;
        token_id: nat;
        amount: nat;
      }>;
    }>;
    s: {
      owner: address;
      permits: address;
      token_metadata: BigMap<
        nat,
        {
          0: nat;
          1: MMap<nat, bytes>;
        }
      >;
      ledger: BigMap<
        {
          0: address;
          1: nat;
        },
        nat
      >;
      operator: BigMap<
        {
          0: address;
          1: nat;
          2: address;
        },
        unit
      >;
      operator_for_all: BigMap<
        {
          0: address;
          1: address;
        },
        unit
      >;
      paused: boolean;
    };
  }) => Promise<void>;
  update_operators: (params: {
    sender: address;
    p: Array<
      | {
          add_operator: {
            owner: address;
            operator: address;
            token_id: nat;
          };
        }
      | {
          remove_operator: {
            owner: address;
            operator: address;
            token_id: nat;
          };
        }
    >;
    s: {
      owner: address;
      permits: address;
      token_metadata: BigMap<
        nat,
        {
          0: nat;
          1: MMap<nat, bytes>;
        }
      >;
      ledger: BigMap<
        {
          0: address;
          1: nat;
        },
        nat
      >;
      operator: BigMap<
        {
          0: address;
          1: nat;
          2: address;
        },
        unit
      >;
      operator_for_all: BigMap<
        {
          0: address;
          1: address;
        },
        unit
      >;
      paused: boolean;
    };
  }) => Promise<void>;
  balance_of: (params: {
    request: Array<{
      owner: address;
      token_id: nat;
    }>;
    callback: address;
    owner: address;
    permits: address;
    token_metadata: BigMap<
      nat,
      {
        0: nat;
        1: MMap<nat, bytes>;
      }
    >;
    ledger: BigMap<
      {
        0: address;
        1: nat;
      },
      nat
    >;
    operator: BigMap<
      {
        0: address;
        1: nat;
        2: address;
      },
      unit
    >;
    operator_for_all: BigMap<
      {
        0: address;
        1: address;
      },
      unit
    >;
    paused: boolean;
  }) => Promise<void>;
  mint: (params: {
    sender: address;
    p: {
      mp_to: address;
      mp_token_id: nat;
      mp_token_amount: nat;
    };
    s: {
      owner: address;
      permits: address;
      token_metadata: BigMap<
        nat,
        {
          0: nat;
          1: MMap<nat, bytes>;
        }
      >;
      ledger: BigMap<
        {
          0: address;
          1: nat;
        },
        nat
      >;
      operator: BigMap<
        {
          0: address;
          1: nat;
          2: address;
        },
        unit
      >;
      operator_for_all: BigMap<
        {
          0: address;
          1: address;
        },
        unit
      >;
      paused: boolean;
    };
  }) => Promise<void>;
  default: (params: {
    pe_entrypoint: int;
    pe_sender: address;
    pe_params: bytes;
    owner: address;
    permits: address;
    token_metadata: BigMap<
      nat,
      {
        0: nat;
        1: MMap<nat, bytes>;
      }
    >;
    ledger: BigMap<
      {
        0: address;
        1: nat;
      },
      nat
    >;
    operator: BigMap<
      {
        0: address;
        1: nat;
        2: address;
      },
      unit
    >;
    operator_for_all: BigMap<
      {
        0: address;
        1: address;
      },
      unit
    >;
    paused: boolean;
  }) => Promise<void>;
};

type contractTypes = {
  methods: Methods;
  methodsObject: MethodsObject;
  storage: Storage;
  code: { __type: "Fa2LogicCode"; protocol: string; code: object[] };
};
export type Fa2LogicContractType = ContractAbstractionFromContractType<contractTypes>;
export type Fa2LogicWalletType = WalletContractAbstractionFromContractType<contractTypes>;
