
import { ContractAbstractionFromContractType, WalletContractAbstractionFromContractType } from './type-utils';
import { address, BigMap, bytes, key, MMap, nat, signature, timestamp } from './type-aliases';

export type Storage = {
    owner: address;
    owner_candidate: {Some: address} | null;
    paused: boolean;
    consumer: Array<address>;
    permits: BigMap<address, {
        counter: nat;
        user_expiry: {Some: nat} | null;
        user_permits: MMap<bytes, {
            expiry: {Some: nat} | null;
            created_at: timestamp;
        }>;
    }>;
    default_expiry: nat;
    metadata: BigMap<string, bytes>;
};

type Methods = {
    declare_ownership: (param: address) => Promise<void>;
    claim_ownership: () => Promise<void>;
    pause: () => Promise<void>;
    unpause: () => Promise<void>;
    set_metadata: (
        k: string,
        d: {Some: bytes} | null,
    ) => Promise<void>;
    add: (param: address) => Promise<void>;
    remove: (param: address) => Promise<void>;
    set_expiry: (
        iv: {Some: nat} | null,
        ip: {Some: bytes} | null,
    ) => Promise<void>;
    setExpiry: (
        u: address,
        sec: nat,
        data: {Some: bytes} | null,
    ) => Promise<void>;
    set_default_expiry: (param: nat) => Promise<void>;
    permit: (
        signer: key,
        sig: signature,
        permit_key: bytes,
    ) => Promise<void>;
    consume: (
        user: address,
        permit_key: bytes,
        err: string,
    ) => Promise<void>;
    check: (
        signer: key,
        sig: signature,
        data: bytes,
    ) => Promise<void>;
};

type MethodsObject = {
    declare_ownership: (param: address) => Promise<void>;
    claim_ownership: () => Promise<void>;
    pause: () => Promise<void>;
    unpause: () => Promise<void>;
    set_metadata: (params: {
        k: string,
        d: {Some: bytes} | null,
    }) => Promise<void>;
    add: (param: address) => Promise<void>;
    remove: (param: address) => Promise<void>;
    set_expiry: (params: {
        iv: {Some: nat} | null,
        ip: {Some: bytes} | null,
    }) => Promise<void>;
    setExpiry: (params: {
        u: address,
        sec: nat,
        data: {Some: bytes} | null,
    }) => Promise<void>;
    set_default_expiry: (param: nat) => Promise<void>;
    permit: (params: {
        signer: key,
        sig: signature,
        permit_key: bytes,
    }) => Promise<void>;
    consume: (params: {
        user: address,
        permit_key: bytes,
        err: string,
    }) => Promise<void>;
    check: (params: {
        signer: key,
        sig: signature,
        data: bytes,
    }) => Promise<void>;
};

type contractTypes = { methods: Methods, methodsObject: MethodsObject, storage: Storage, code: { __type: 'PermitsCode', protocol: string, code: object[] } };
export type PermitsContractType = ContractAbstractionFromContractType<contractTypes>;
export type PermitsWalletType = WalletContractAbstractionFromContractType<contractTypes>;
