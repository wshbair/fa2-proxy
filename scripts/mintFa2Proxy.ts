import { InMemorySigner } from "@taquito/signer";
import { TezosToolkit } from "@taquito/taquito";
import * as att from "@completium/archetype-ts-types";
import { pack } from "@completium/experiment-ts";

import { Fa2ProxyContractType } from "../types/fa2_proxy.types";
import { tas } from "../types/type-aliases";
import { mint_param, mint_param_mich_type } from "../tests/binding/fa2_types";
import { Mint } from "../tests/binding/fa2_proxy";

const RPC_URL = "https://ghostnet.tezos.marigold.dev";
const SIGNER_SK = "edsk4QLrcijEffxV31gGdN2HU7UpyJjA8drFoNcmnB28n89YjPNRFm"; // DO NOT USE IN PRODUCTION

const interact = async () => {
  try {
    const Tezos = new TezosToolkit(RPC_URL);
    Tezos.setSignerProvider(new InMemorySigner(SIGNER_SK));
    const pkh = await Tezos.signer.publicKeyHash();
    console.log("Signer: ", pkh);

    const arg = process.argv.slice(2)[0];
    if (!arg) {
      throw Error("You need to provide the FA2 Proxy Contract address as an argument");
    }

    const fa2ProxyContract = await Tezos.contract.at<Fa2ProxyContractType>(arg);

    // Packing parameters for the FA2 Logic contract
    const args = pack(
      new mint_param(new att.Address(pkh), new att.Nat(0), new att.Nat(1)).to_mich(),
      mint_param_mich_type,
    ).toString();

    // Entrypoint to call
    const entry: any = new Mint().to_mich();

    const op = await fa2ProxyContract.methodsObject
      .exec({ ep_entrypoint: tas.int(entry.int), ep_params: tas.bytes(args) })
      .send();
    await op.confirmation(2);

    console.log("Success");
  } catch (err) {
    console.log(err);
  }
};
interact();
