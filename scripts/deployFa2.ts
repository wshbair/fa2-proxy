import { InMemorySigner } from "@taquito/signer";
import { TezosToolkit } from "@taquito/taquito";
import { Fa2ProxyContractType } from "../types/fa2_proxy.types";
import { Fa2ProxyCode } from "../types/fa2_proxy.code";
import { tas } from "../types/type-aliases";
import { PermitsContractType } from "../types/permits.types";
import { PermitsCode } from "../types/permits.code";

const RPC_URL = "https://ghostnet.tezos.marigold.dev";
const SIGNER_SK = "edsk4QLrcijEffxV31gGdN2HU7UpyJjA8drFoNcmnB28n89YjPNRFm";

const deployFa2 = async () => {
  const Tezos = new TezosToolkit(RPC_URL);
  Tezos.setSignerProvider(new InMemorySigner(SIGNER_SK));
  const pkh = await Tezos.signer.publicKeyHash();
  console.log("Signer: ", pkh);

  console.log("Deploying Permits contract...");
  const permits = await Tezos.contract.originate<PermitsContractType>({
    code: PermitsCode.code,
    storage: {
      owner: tas.address(pkh),
      owner_candidate: null,
      paused: false,
      consumer: [],
      permits: tas.bigMap([]),
      default_expiry: tas.nat(0),
      metadata: tas.bigMap([]),
    },
  });
  await permits.confirmation(2);
  console.log("Permits contract deployed at: ", permits.contractAddress);

  console.log("Deploying Fa2Proxy contract...");
  const fa2Proxy = await Tezos.contract.originate<Fa2ProxyContractType>({
    code: Fa2ProxyCode.code,
    storage: {
      owner: tas.address(pkh),
      permits: tas.address(permits.contractAddress!),
      token_metadata: tas.bigMap([]),
      ledger: tas.bigMap([]),
      operator: tas.bigMap([]),
      operator_for_all: tas.bigMap([]),
      paused: false,
      proxy_address: tas.address(pkh), // Dummy value
      metadata: tas.bigMap([]),
    },
  });
  await fa2Proxy.confirmation(2);
  console.log("FA2 proxy contract deployed at: ", fa2Proxy.contractAddress);
};
deployFa2();
