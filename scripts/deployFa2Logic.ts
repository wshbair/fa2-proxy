import { InMemorySigner } from "@taquito/signer";
import { TezosToolkit } from "@taquito/taquito";
import { Fa2ProxyContractType } from "../types/fa2_proxy.types";
import { PermitsContractType } from "../types/permits.types";
import { tas } from "../types/type-aliases";
import { Fa2LogicContractType } from "../types/fa2_logic.types";
import { Fa2LogicCode } from "../types/fa2_logic.code";

const RPC_URL = "https://ghostnet.tezos.marigold.dev";
const SIGNER_SK = "edsk4QLrcijEffxV31gGdN2HU7UpyJjA8drFoNcmnB28n89YjPNRFm";

const deployFa2Logic = async () => {
  try {
    const Tezos = new TezosToolkit(RPC_URL);
    Tezos.setSignerProvider(new InMemorySigner(SIGNER_SK));
    const pkh = await Tezos.signer.publicKeyHash();
    console.log("Signer: ", pkh);

    const proxyAddress = process.argv.slice(2)[0];
    if (!proxyAddress) {
      throw Error("You need to provide the FA2 Proxy Contract address as an argument");
    }
    const permitsAddress = process.argv.slice(2)[1];
    if (!proxyAddress) {
      throw Error("You need to provide the Permits Contract address as an argument");
    }

    console.log("Deploying FA2 Logic contract...");
    const fa2Logic = await Tezos.contract.originate<Fa2LogicContractType>({
      code: Fa2LogicCode.code,
      storage: tas.address(proxyAddress),
    });
    await fa2Logic.confirmation(2);
    console.log("FA2 logic contract deployed at: ", fa2Logic.contractAddress);

    console.log("Adding FA2 Logic to the FA2 Proxy contract...");
    const fa2ProxyContract = await Tezos.contract.at<Fa2ProxyContractType>(proxyAddress);
    await fa2ProxyContract.methods.set_proxy_address(tas.address(fa2Logic.contractAddress!)).send();

    console.log("Adding FA2 Logic as Permits consumer...");
    const permitsContract = await Tezos.contract.at<PermitsContractType>(permitsAddress);
    await permitsContract.methods.add(tas.address(fa2Logic.contractAddress!)).send();

    console.log("Success");
  } catch (err) {
    console.log(err);
  }
};
deployFa2Logic();
